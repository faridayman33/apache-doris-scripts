import requests
#import Teleg
from pyspark.sql import functions as F
from pyspark.sql import SparkSession
from pyspark.context import SparkContext
from pyspark.sql.functions import *
from pyspark.sql.functions import col, json_tuple
from datetime import datetime, timedelta
from dotenv import load_dotenv
import pyspark
import os
import threading
import random
import pyspark.sql.functions
import requests
import pyspark
from pyspark.sql import SparkSession 
from pyspark.sql.functions import *
import os
from dotenv import load_dotenv
from datetime import datetime, timedelta
import pydoris
from pyspark.sql.types import *
try:
    import jaydebeapi
except ImportError:
    import sys
    import subprocess
    subprocess.check_call([sys.executable, "-m", "pip", "install", "jaydebeapi"])
    import jaydebeapi
    


#-----------------------Starting spark session---------------------------------------------- 
spark = SparkSession \
    .builder \
    .appName('ETL_ODS_all') \
    .enableHiveSupport() \
    .config("spark.executor.memory", "20g") \
    .config("spark.sql.shuffle.partitions", "200") \
    .config("spark.default.parallelism", "200") \
    .getOrCreate()
#spark.sparkContext.setLogLevel("all")
#-----------------------------------------------------------------------------------------------------------
ODS_Queries = {
    # GW Services Provider
    'SERVICE_PROVIDER': '''SELECT  ID ,  Name , 'GW' as  Source  FROM STAGING.GW_PROD_SERVICEPROVIDER''',
    # TMS Services Provider
    'TMS_SERVICE_PROVIDER': '''SELECT  ID ,  Name , 'GW_V2' as  Source  FROM STAGING.TMS_PROD_ServiceProviders''',
    # GW Parent Group
    'PARENT_GROUPS': '''SELECT  ID ,  NAME  FROM STAGING.GW_PROD_PARENTGROUPS''',
    # GW Service Group
    'SERVICE_GROUPS': '''SELECT  ID ,  Name ,  ArName ,  ParentGroupID  as Parent_Group_ID FROM STAGING.GW_PROD_SERVICEGROUPS''',
    # GW Service
    'SERVICE': '''SELECT  ID ,  Name ,  ServiceTypeID  as Service_Type_ID,  ServiceCategoryID  as Service_Category_ID,  Code , 
                 ServiceEntityID  as Service_Entity_ID,  ServiceGroupID  as Service_Group_ID
                FROM STAGING.GW_PROD_SERVICE''',
    # GW Services
    'SERVICES': '''SELECT  Id ,  ProviderId ,  Name ,  Active  FROM STAGING.GW_PROD_SERVICES''',
    # GW TransactionType
    'TRANSACTIONS_TYPES': '''SELECT * FROM STAGING.GW_PROD_TRANSACTIONS_TYPES''',
    # GW Provider
    'PROVIDER': '''SELECT * FROM STAGING.GW_PROD_PROVIDER''',
    # GW PaymentType
    'PAYMENT_TYPE': '''SELECT  ID ,  Name ,  ArName  FROM STAGING.GW_PROD_PAYMENTTYPE''',
    # GW Denomination
    'DENOMINATIONS': '''SELECT  ID ,  Name ,  Value ,  ServiceID  as Service_ID,  Status ,  PaymentTypeID ,  Inquirable 
                      FROM STAGING.GW_PROD_DENOMINATIONS''',
    # GW Denomination service provider
    'DENOMINATION_SERVICE_PROVIDER': '''SELECT  ID ,  DenominationID  as Denomination_ID,  ServiceProviderID  as Service_Provider_ID,  Status , 
                                        OldServiceId  as Old_Service_Id,  ProviderAmount , 'GW' as  Source 
                                       FROM STAGING.GW_PROD_DENOMINATIONSERVICEPROVIDER''',
    # TMS Denomination Service provider
    'TMS_DENOMINATION_SERVICE_PROVIDER': '''SELECT ID, Denomination_ID, Service_Provider_ID, Status, Old_Service_Id, ProviderAmount, Source
FROM (
    SELECT D.ID, D.DenominationID as Denomination_ID, D.ServiceProviderID as Service_Provider_ID,
    D.Status, 0 as Old_Service_Id, ProviderAmount, 'GW_V2' as Source,
    ROW_NUMBER() OVER(PARTITION BY D.DenominationID, COALESCE(D.ServiceProviderID, -1) ORDER BY COALESCE(D.Status, 0) DESC) RN
    FROM STAGING.TMS_PROD_DenominationServiceProviders D
) AS sub
WHERE RN = 1''',
    # GW place
    'PLACE': '''SELECT  Id ,  PlaceName  as Place_Name,  GovernorateId  as Governorate_Id,  PlaceType  as Place_Type
                FROM STAGING.GW_PROD_PLACE''',
    # GW Account Type
    'ACCOUNT_TYPE': '''SELECT  ID ,  Name ,  Status  FROM STAGING.GW_PROD_ACCOUNTTYPE''',
    # GW 
    
    'ACCOUNTS': '''SELECT  Id ,  CenterName  as name,  Address ,  Mobile ,  Active ,  added_date ,
                     national_id ,  Place_Id ,  Parent_CenterID  as Commissionable,  AccountTypeID  as Account_Type_ID,  sales_person ,
                     OwnerName  as  Owner_Name ,  Parent_CenterID  as  ParentID 
                    FROM STAGING.GW_PROD_CENTERS''',
    # GW Account_Location
    'ACCOUNT_LOCATIONS': '''SELECT  ID ,  AccountID  as Account_ID,  Longitude ,  Latitude ,  CreationDate  as Creation_Date, 
                             UpdateDate  as Update_Date
                            FROM STAGING.GW_PROD_ACCOUNTLOCATIONS''',
    # Request Status
    'REQUEST_STATUS': '''SELECT  ID ,  Description ,  ResponseCode  as Response_Code,  DescriptionAr  as Description_Ar
                        FROM STAGING.GW_PROD_REQUESTSTATUS''',
    # GW Service Fields
    'SERVICE_FIELDS': '''SELECT  SFId  as ID,  FieldName  as Field_Name,  ServId 
                        FROM STAGING.GW_PROD_SERVICESFIELDS''',
    # GW Invoice
    'INVOICE': '''SELECT  Id ,  Invoice_Id_card ,  ServId ,  TotalPrice ,  UserId ,  AddedTime ,  Status ,  InCome ,  added_money ,  Basci_val ,
                 Provider_Response ,  service_id ,  ProviderTransactionId ,  ProviderCardTransactionId ,  ECardmomknPaymentId ,
                 ClientPhone , 'GW' as  Source ,  ClientName ,  Code 
                FROM STAGING.GW_PROD_INVOICE 
                WHERE  AddedTime  BETWEEN STR_TO_DATE('{}', '%Y-%m-%d %H:%i:%s') AND STR_TO_DATE('{}', '%Y-%m-%d %H:%i:%s') ''',
#--------------------------------------------UAT INVOICE-------------------------------------------------
'UAT_INVOICE': '''SELECT  Id , 
 Invoice_Id_card , 
 ServId , 
 TotalPrice , 
 UserId , 
 AddedTime , 
 Status , 
 InCome , 
 added_money , 
 Basci_val , 
 Provider_Response , 
 service_id , 
 ProviderTransactionId , 
 ProviderCardTransactionId , 
 ECardmomknPaymentId ,
 ClientPhone ,
'UAT' as  Source ,
 ClientName ,
 Code 
FROM STAGING.GW_UAT_INVOICE WHERE  AddedTime  BETWEEN STR_TO_DATE('{}', '%Y-%m-%d %H:%i:%s') AND STR_TO_DATE('{}', '%Y-%m-%d %H:%i:%s')  ''' ,
#--------------------------------------------GW Invoice Dettails------------------------------------------
'INVOICE_DETAILS': '''
        SELECT IDS.Id as ID, IDS.InvId as Invoice_Id, IDS.SFId as Service_Field_Id, IDS.Value, 'GW' as Source
        FROM STAGING.GW_PROD_INVOICEDETAILS IDS 
        JOIN STAGING.GW_PROD_INVOICE I ON I.Id = IDS.InvId
        WHERE I.AddedTime BETWEEN STR_TO_DATE('{}', '%Y-%m-%d %H:%i:%s') AND STR_TO_DATE('{}', '%Y-%m-%d %H:%i:%s')
    ''',
#-------------------------------------------UAT Invoice Details-----------------------------------------
'UAT_INVOICE_DETAILS': '''
        SELECT IDS.Id as ID, IDS.InvId as Invoice_Id, IDS.SFId as Service_Field_Id, IDS.Value, 'UAT' as Source
        FROM STAGING.GW_UAT_INVOICEDETAILS IDS 
        JOIN STAGING.GW_UAT_INVOICE I ON I.Id = IDS.InvId
        WHERE I.AddedTime BETWEEN STR_TO_DATE('{}', '%Y-%m-%d %H:%i:%s') AND STR_TO_DATE('{}', '%Y-%m-%d %H:%i:%s')
    ''' , 
#---------------------------------------------------GW Electronic Charge----------------------------------
'ELECTRONIC_CHARGE' : '''   
SELECT  Id , 
 provider_company , 
 mobile_number , 
 value , 
 invoice_price , 
 status_transfer , 
 UserId , 
 CenterId , 
 AddedTime , 
 provider , 
 ServId , 
 InCome , 
 Log_Id ,
'GW' as  Source 
FROM STAGING.GW_PROD_ELECTRONIC_CHARGE WHERE  AddedTime  BETWEEN STR_TO_DATE('{}', '%Y-%m-%d %H:%i:%s') AND STR_TO_DATE('{}', '%Y-%m-%d %H:%i:%s')  ''',
#-------------------------------------------------UAT ELECTRONICCHARGE-----------------------------------
'UAT_ELECTRONIC_CHARGE': ''' SELECT  Id , 
 provider_company , 
 mobile_number , 
 value , 
 invoice_price , 
 status_transfer , 
 UserId , 
 CenterId , 
 AddedTime , 
 provider , 
 ServId , 
 InCome , 
 Log_Id ,
'UAT'as  Source 
FROM STAGING.GW_UAT_ELECTRONIC_CHARGE WHERE  AddedTime  BETWEEN STR_TO_DATE('{}', '%Y-%m-%d %H:%i:%s') AND STR_TO_DATE('{}', '%Y-%m-%d %H:%i:%s')
  ''' ,
#-------------------------------------------------GW Electronic Charge Voucher----------------------------
'ELECTRONIC_CHARGE_VOUCHER' : '''SELECT  Id , 
 provider_company , 
 value , 
 invoice_price , 
 status_transfer , 
 UserId , 
 CenterId , 
 AddedTime ,
 masary_trans_id , 
 voucher_number , 
 serial_number ,
 provider , 
 ServId , 
 InCome , 
 Log_Id ,  charge_status  ,
'GW' as  Source ,
 RequestID ,
 DenominationID 
FROM STAGING.GW_PROD_ELECTRONIC_CHARGE_VOUCHER WHERE  AddedTime  BETWEEN STR_TO_DATE('{}', '%Y-%m-%d %H:%i:%s') AND STR_TO_DATE('{}', '%Y-%m-%d %H:%i:%s') ''' ,
#--------------------------------------------UAT electronic charge voucher-----------------------------------
'UAT_ELECTRONIC_CHARGE_VOUCHER': '''SELECT  Id , 
 provider_company , 
 value , 
 invoice_price , 
 status_transfer , 
 UserId , 
 CenterId , 
 AddedTime ,
 masary_trans_id , 
 voucher_number , 
 serial_number ,
 provider , 
 ServId , 
 InCome , 
 Log_Id ,  charge_status ,
'UAT' as  Source ,
 RequestID ,
 DenominationID 
FROM STAGING.GW_UAT_ELECTRONIC_CHARGE_VOUCHER WHERE  AddedTime  BETWEEN STR_TO_DATE('{}', '%Y-%m-%d %H:%i:%s') AND STR_TO_DATE('{}', '%Y-%m-%d %H:%i:%s') 
    ''' ,
#--------------------------------------------GW Channel Type --------------------------------------------------
'CHANNEL_TYPES' : '''SELECT  ID , Name , ChannelCategoryID 
FROM STAGING.GW_PROD_CHANNELTYPES ''',
#------------------------------------------------GW Request----------------------------------------------------------
'REQUESTS': ''' SELECT ID, 
    UUID, 
    AccountID AS Account_ID, 
    ServiceDenominationID AS Service_Denomination_ID, 
    Status,
    RequestDate AS Request_Date, 
    ResponseDate AS Response_Date,
    Amount, 
    BillingAccount AS Billing_Account, 
    ChannelID AS Channel_ID, 
    ProviderServiceRequestID AS Provider_Service_Request_ID,
    0 AS Ref_Number, 
    1 AS Payment_Method, 
    'unknown' AS CompositeTransactionID, 
    'GW' AS SourceSystem,
    UserID
FROM STAGING.GW_PROD_REQUESTS
WHERE RequestDate  BETWEEN STR_TO_DATE('{}', '%Y-%m-%d %H:%i:%s') AND STR_TO_DATE('{}', '%Y-%m-%d %H:%i:%s')
 ''' ,
#------------------------------------------- UAT Requests ----------------------------------------
'UAT_REQUESTS': '''SELECT  ID ,  UUID ,  AccountID  as Account_ID,  ServiceDenominationID  as Service_Denomination_ID,  Status ,
            RequestDate  as Request_Date,  ResponseDate  as Response_Date,
         Amount ,  BillingAccount  as Billing_Account,  ChannelID  as Channel_ID,  ProviderServiceRequestID  as Provider_Service_Request_ID,
         0 as Ref_Number  , 1 as  Payment_Method  , 'unknown' as CompositeTransactionID   , 'UAT' as  SourceSystem , UserID 
FROM STAGING.GW_UAT_REQUESTS WHERE  RequestDate  BETWEEN STR_TO_DATE('{}', '%Y-%m-%d %H:%i:%s') AND STR_TO_DATE('{}', '%Y-%m-%d %H:%i:%s')
    ''' ,
#--------------------------------------------GW Channel ----------------------------------------------
'CHANNELS': '''
SELECT  ID , 
 Name , 
 ChannelTypeID , 
 ChannelOwnerID , 
 Serial , 
 PaymentMethodID  
FROM STAGING.GW_PROD_CHANNELS  ''',
#--------------------------------------------Gw Account Channel -------------------------------------
'ACCOUNT_CHANNELS': ''' 
SELECT  ID , 
         AccountID , 
         ChannelID , 
         CreationDate , 
         CreatedBy , 
         Status , 
         UpdateDate , 
         UpdatedBy 
FROM STAGING.GW_PROD_ACCOUNTCHANNELS ''',
#---------------------------------------------------GW Transaction ------------------------------------
'TRANSACTIONS': '''SELECT T.ID, AccountIDFrom as Account_ID_From, AccountIDTo as Account_ID_To, TotalAmount as Total_Amount, 
            TransactionType as Transaction_Type, IsReversed as Is_Reversed, OriginalTrx as Original_Trx, Date,
            OriginalAmount as Original_Amount, Fees, 0 as Taxes, InvoiceID as Invoice_ID, RequestID as Request_ID, 'GW' as SourceSystem
        FROM STAGING.GW_PROD_TRANSACTIONS T 
        WHERE T.Date BETWEEN STR_TO_DATE('{}', '%Y-%m-%d %H:%i:%s') AND STR_TO_DATE('{}', '%Y-%m-%d %H:%i:%s')   ''' ,
#-------------------------------------------------------UAT Transaction---------------------------------
'UAT_TRANSACTIONS': '''SELECT T.ID, AccountIDFrom as Account_ID_From, AccountIDTo as Account_ID_To, TotalAmount as Total_Amount, 
            TransactionType as Transaction_Type, IsReversed as Is_Reversed, OriginalTrx as Original_Trx, Date,
            OriginalAmount as Original_Amount, Fees, 0 as Taxes, InvoiceID as Invoice_ID, RequestID as Request_ID, 'UAT' as SourceSystem
        FROM STAGING.GW_UAT_TRANSACTIONS T 
        WHERE T.Date BETWEEN STR_TO_DATE('{}', '%Y-%m-%d %H:%i:%s') AND STR_TO_DATE('{}', '%Y-%m-%d %H:%i:%s') 
 ''' ,
#-------------------------------------------------------GW Balance ------------------------------------
'BALANCE' : ''' 
SELECT  Id , 
 CenterId , 
 Amount , 
 Type , 
 Descrp , 
 AddedTime ,  
 InvId , 
 Before , 
 ServId , 
 trans_id ,
 tel  ,
'GW' as  Source ,
 UserId 
FROM STAGING.GW_PROD_BALANCE WHERE  AddedTime  BETWEEN STR_TO_DATE('{}', '%Y-%m-%d %H:%i:%s') AND STR_TO_DATE('{}', '%Y-%m-%d %H:%i:%s') ''',
#-----------------------------------------------UAT Balance ------------------------------------------------
'UAT_BALANCE': '''SELECT Id, 
CenterId, 
Amount, 
Type, 
Descrp, 
AddedTime, 
InvId, 
Before, 
ServId, 
trans_id,
tel ,
"UAT"  as Source,
UserId
FROM STAGING.GW_UAT_BALANCE WHERE AddedTime BETWEEN STR_TO_DATE('{}', '%Y-%m-%d %H:%i:%s') AND STR_TO_DATE('{}', '%Y-%m-%d %H:%i:%s')  ''' ,
#-----------------------------------------GW Account Transaction Commission-----------------------------------
'ACCOUNT_TRANSACTION_COMMISSION' : ''' SELECT AC.ID, AC.TransactionID AS Transaction_ID, AC.Commission, 'GW' as Source
        FROM STAGING.GW_PROD_ACCOUNTTRANSACTIONCOMMISSION AC
        JOIN STAGING.GW_PROD_TRANSACTIONS T ON T.ID = AC.TransactionID
        WHERE T.Date BETWEEN STR_TO_DATE('{}', '%Y-%m-%d %H:%i:%s') AND STR_TO_DATE('{}', '%Y-%m-%d %H:%i:%s') 
 ''' ,
#------------------------------------------UAT Account Transaction commission-----------------------------------
'UAT_ACCOUNT_TRANSACTION_COMMISSION': '''SELECT AC.ID, AC.TransactionID AS Transaction_ID, AC.Commission, 'UAT' as Source
        FROM STAGING.GW_UAT_ACCOUNTTRANSACTIONCOMMISSION AC
        JOIN STAGING.GW_UAT_TRANSACTIONS T ON T.ID = AC.TransactionID 
        WHERE T.Date BETWEEN STR_TO_DATE('{}', '%Y-%m-%d %H:%i:%s') AND STR_TO_DATE('{}', '%Y-%m-%d %H:%i:%s') 
  ''' ,
#------------------------------------------GW Channel Identifier--------------------------------------------------
'CHANNEL_IDENTIFIERS': '''SELECT  ID ,
 ChannelID , 
 Value ,
 CreationDate , 
 CreatedBy , 
 Status , 
 UpdateDate , 
 UpdatedBy 
FROM STAGING.GW_PROD_CHANNELIDENTIFIERS ''',
#------------------------------------------Center Statments------------------------------------------------
'CENTERS_STATEMENT':'''SELECT 
    s.Id AS ID, 
    s.CenterId AS AccountID,
    CAST(a.Name AS VARCHAR(255)) AS CenterType,
    CAST(c.CenterName AS VARCHAR(500)) AS CenterName,
    s.Balance AS ClosingBalance,
    DATE_FORMAT(s.AddedTime, '%Y-%m-%d') AS CreationDate,
    s.AddedTime AS AddedTime,
    'GW' AS SourceSystem
FROM STAGING.GW_PROD_CENTERS_STATEMENT s
JOIN STAGING.GW_PROD_CENTERS c ON s.CenterId = c.Id
JOIN STAGING.GW_PROD_ACCOUNTTYPE a ON c.AccountTypeID = a.ID
WHERE DATE_FORMAT(s.AddedTime, '%Y-%m-%d') = DATE_FORMAT(NOW() - INTERVAL 1 DAY, '%Y-%m-%d') 

UNION ALL 

SELECT 
    s.Id AS ID, 
    s.CenterId AS AccountID,
    CAST(a.Name AS VARCHAR(255)) AS CenterType,
    CAST(c.CenterName AS VARCHAR(500)) AS CenterName,
    s.Balance AS ClosingBalance,
    DATE_FORMAT(s.AddedTime, '%Y-%m-%d') AS CreationDate,
    s.AddedTime AS AddedTime,
    'UAT' AS SourceSystem
FROM STAGING.GW_UAT_CENTERS_STATEMENT s
JOIN STAGING.GW_PROD_CENTERS c ON s.CenterId = c.Id
JOIN STAGING.GW_PROD_ACCOUNTTYPE a ON c.AccountTypeID = a.ID
WHERE DATE_FORMAT(s.AddedTime, '%Y-%m-%d') = DATE_FORMAT(NOW() - INTERVAL 1 DAY, '%Y-%m-%d')

UNION ALL 

SELECT 
    ID,
    AccountID,
    CAST(AccountType AS VARCHAR(255)), 
    CAST(AccountName AS VARCHAR(500)),
    Balance, 
    DATE(AddedTime) AS CreationDate, 
    AddedTime, 
    'GW_V2' AS SourceSystem
FROM STAGING.ReportsDB_Centers_Statement S
WHERE DATE(AddedTime) = CURDATE()

UNION ALL 

SELECT 
    ID,
    AccountID,
    CAST(AccountType AS VARCHAR(255)), 
    CAST(AccountName AS VARCHAR(500)),
    Balance, 
    DATE(AddedTime) AS CreationDate, 
    AddedTime, 
    'UAT_V2' AS SourceSystem
FROM STAGING.ReportsDB_UAT_Centers_Statement S
WHERE DATE(AddedTime) = CURDATE()

 '''   ,

#-------------------------------------------GW LOGS-----------------------------------------------------------
#'LOGS':'''SELECT  ID  ,  Date  , Log  ,  ProviderServiceRequestID  as  Provider_Service_RequestID  , LogTypeID  FROM   STAGING . GW_PROD_LOGS  WHERE  Date  BETWEEN STR_TO_DATE('{}', '%Y-%m-%d %H:%i:%s') AND STR_TO_DATE('{}', '%Y-%m-%d %H:%i:%s')  ''',
#--------------------------------------------UAT LOGS-------------------------------------------------------
 #'UAT_LOGS':   ''' SELECT  ID  ,  Date  , Log  ,  ProviderServiceRequestID  as  Provider_Service_RequestID  , LogTypeID  FROM   STAGING . GW_UAT_LOGS  WHERE  Date  BETWEEN STR_TO_DATE('{}', '%Y-%m-%d %H:%i:%s') AND STR_TO_DATE('{}', '%Y-%m-%d %H:%i:%s') ''' , 
#-----------------------------------------------TMS Request ---------------------------------------------------
'TMS_REQUESTS' : ''' SELECT  ID ,  UUID ,  AccountID  as Account_ID,  ServiceDenominationID  as Service_Denomination_ID,  StatusID  as Status, 
            CreationDate  as Request_Date,  ResponseDate  as Response_Date,
         Amount ,  BillingAccount  as Billing_Account,  ChannelID  as Channel_ID,  ProviderServiceRequestID  as Provider_Service_Request_ID ,  RefNumber  as Ref_Number  , PaymentMethod  as  Payment_Method ,
       COMPOSITETRANSACTIONID  as CompositeTransactionID , 'GW_V2' as  SourceSystem , UserID 
FROM STAGING.TMS_PROD_Requests WHERE  CreationDate  BETWEEN STR_TO_DATE('{}', '%Y-%m-%d %H:%i:%s') AND STR_TO_DATE('{}', '%Y-%m-%d %H:%i:%s') ''' ,
#-----------------------------------------------UAT TMS Request---------------------------------------------------
'UAT_TMS_REQUESTS': '''SELECT  ID ,  UUID ,  AccountID  as Account_ID,  ServiceDenominationID  as Service_Denomination_ID,  StatusID  as Status, 
            CreationDate  as Request_Date,  ResponseDate  as Response_Date,
         Amount ,  BillingAccount  as Billing_Account,  ChannelID  as Channel_ID,  ProviderServiceRequestID  as Provider_Service_Request_ID ,  RefNumber  as Ref_Number  , PaymentMethod  as  Payment_Method ,
        CompositeTransactionID  , 'UAT_V2' as  SourceSystem , UserID 
FROM STAGING.TMS_UAT_Requests WHERE  CreationDate  BETWEEN STR_TO_DATE('{}', '%Y-%m-%d %H:%i:%s') AND STR_TO_DATE('{}', '%Y-%m-%d %H:%i:%s')  ''' ,
#----------------------------------------------------TMS Transaction---------------------------------------------------
'TMS_TRANSACTIONS' : '''SELECT T.ID, AccountIDFrom as Account_ID_From, AccountIDTo as Account_ID_To, TotalAmount as Total_Amount, 
            TransactionType as Transaction_Type, IsReversed as Is_Reversed, OriginalTrx as Original_Trx, CreationDate as Date,
            OriginalAmount as Original_Amount, Fees, Taxes, InvoiceID as Invoice_ID, RequestID as Request_ID, 'GW_V2' as SourceSystem
        FROM STAGING.TMS_PROD_Transactions T 
        WHERE T.CreationDate BETWEEN STR_TO_DATE('{}', '%Y-%m-%d %H:%i:%s') AND STR_TO_DATE('{}', '%Y-%m-%d %H:%i:%s') ''' ,
#-----------------------------------------------------UAT TMS Transaction -------------------------------------
'UAT_TMS_TRANSACTIONS' : '''SELECT T.ID, AccountIDFrom as Account_ID_From, AccountIDTo as Account_ID_To, TotalAmount as Total_Amount, 
            TransactionType as Transaction_Type, IsReversed as Is_Reversed, OriginalTrx as Original_Trx, CreationDate as Date,
            OriginalAmount as Original_Amount, Fees, Taxes, InvoiceID as Invoice_ID, RequestID as Request_ID, 'UAT_V2' as SourceSystem
        FROM STAGING.TMS_UAT_Transactions T 
        WHERE T.CreationDate BETWEEN STR_TO_DATE('{}', '%Y-%m-%d %H:%i:%s') AND STR_TO_DATE('{}', '%Y-%m-%d %H:%i:%s')   ''' , 
#-------------------------------------------------------ID Accounts ------------------------------------------
'ID_ACCOUNTS': '''SELECT A.ID, A.Name, AO.Address, AO.Mobile as Mobile, Active, A.CreationDate,
            AO.NationalID as national_id, RegionID as Place_Id, Parent_CenterID as Commissionable,
            ATP.AccountTypeID as Account_Type_ID, 'Unknown' as sales_person, 'Unknown' as Owner_Name,
            Parent_CenterID as ParentID
        FROM STAGING.ID_PROD_CENTERS A
        LEFT JOIN STAGING.ID_PROD_AccountTypeProfiles ATP ON ATP.ID = A.AccountTypeProfileID
        LEFT JOIN STAGING.ID_PROD_Account_Owners AO ON AO.AccountID = A.ID
 '''   ,
 #--------------------------------------------ID Account_Location-------------------------------------------
'ID_ACCOUNT_LOCATIONS' : ''' SELECT R.ID, A.ID as Account_ID , A.Longitude, A.Latitude, R.CreationDate as Creation_Date , 
R.UpdateDate as  Update_Date
FROM STAGING.ID_PROD_Regions R
LEFT JOIN STAGING.ID_PROD_CENTERS A ON  A.RegionID = R.ID ''' , 
#---------------------------------------------ID channel----------------------------------------------------------
'ID_CHANNELS': '''
SELECT  ID , 
 Name , 
 ChannelTypeID , 
 ChannelOwnerID , 
 Serial , 
 PaymentMethodID  
FROM STAGING.ID_PROD_CHANNELS  ''' ,
#------------------------------------------ID Account Owner ----------------------------
'ACCOUNT_OWNERS'  : '''  SELECT * FROM STAGING.ID_PROD_Account_Owners  ''', 
#-------------------------------------------ID channel Identifier ----------------------------------------------
'ID_CHANNEL_IDENTIFIERS': '''
SELECT  ID ,
 ChannelID , 
 Value , 
 CreationDate , 
 CreatedBy , 
 Status , 
 UpdateDate , 
 UpdatedBy 
FROM STAGING.ID_PROD_CHANNElIDENTIFIERS'''   ,
#----------------------------------------TMS Account Tranasction Commission-----------------------------------
'TMS_ACCOUNT_TRANSACTION_COMMISSION' :'''SELECT AC.ID, 
AC.TransactionID AS Transaction_ID,
AC.Commission,
'GW_V2' as Source
FROM STAGING.TMS_PROD_AccountTransactionCommissions AC
JOIN STAGING.TMS_PROD_Transactions T ON T.ID = AC.TransactionID 
Where  T.CreationDate BETWEEN STR_TO_DATE('{}', '%Y-%m-%d %H:%i:%s') AND STR_TO_DATE('{}', '%Y-%m-%d %H:%i:%s') 

 ''' ,
#----------------------------------------TMS UAT Account Transaction Commission ---------------------------------
'TMS_UAT_ACCOUNT_TRANSACTION_COMMISSION' : ''' SELECT AC.ID, 
AC.TransactionID AS Transaction_ID,
AC.Commission,
'UAT_V2' as Source
FROM STAGING.TMS_UAT_AccountTransactionCommissions AC
JOIN STAGING.TMS_UAT_Transactions T ON T.ID = AC.TransactionID 
Where  T.CreationDate BETWEEN STR_TO_DATE('{}', '%Y-%m-%d %H:%i:%s') AND STR_TO_DATE('{}', '%Y-%m-%d %H:%i:%s') 

  ''' , 
#---------------------------------------------ID Account Type --------------------------------------------------
'ID_ACCOUNT_TYPE' : ''' SELECT   ID ,  Name ,  Status   
FROM STAGING.ID_PROD_ACCOUNTTYPE ''' ,   
#------------------------------------------ ID Channel Type ---------------------------------------------------
'ID_CHANNEL_TYPES' : '''SELECT  ID , Name , ChannelCategoryID 
FROM STAGING.ID_PROD_CHANNELTYPES  ''' ,
#------------------------------------------- ID Account Type Profile ---------------------------------------------
'Account_Type_Profile' : ''' SELECT * FROM STAGING.ID_PROD_AccountTypeProfiles '''  ,
#------------------------------------------ID Governarets ---------------------------------------------------
'GOVERNORATES': '''SELECT * FROM STAGING.ID_PROD_Governorates ''' , 
#----------------------------------------------TMS FailedReqStatusCode----------------------------------------
'FailedRequestStatusCodes' : '''SELECT  ID  ,  CreationDate  as Creation_Date ,  Code  ,  Message  , RequestID  
 FROM STAGING.TMS_PROD_FailedRequestStatusCodes WHERE  CreationDate  BETWEEN STR_TO_DATE('{}', '%Y-%m-%d %H:%i:%s') AND STR_TO_DATE('{}', '%Y-%m-%d %H:%i:%s')
 ''' ,
#-----------------------------------------------UAT TMS Failed REquest Status code------------------------------
'UAT_FailedRequestStatusCodes': '''SELECT  ID  ,  CreationDate  as Creation_Date ,  Code  ,  Message  , RequestID  
 FROM STAGING.TMS_UAT_FailedRequestStatusCodes WHERE  CreationDate  BETWEEN STR_TO_DATE('{}', '%Y-%m-%d %H:%i:%s') AND STR_TO_DATE('{}', '%Y-%m-%d %H:%i:%s')  ''' ,
#-----------------------------------------------TMS Inquiry Details------------------------------------------------------
'Inquiry_BILLS_Details' : '''SELECT  ID  ,  CreationDate  as Creation_Date ,  InquiryBillID  , Value , ProviderDenominationParameterID  as Provider_Denomination_Parameter_ID ,
'GW_V2' as  Source 
FROM STAGING.TMS_PROD_InquiryBillDetails WHERE  CreationDate  BETWEEN STR_TO_DATE('{}', '%Y-%m-%d %H:%i:%s') AND STR_TO_DATE('{}', '%Y-%m-%d %H:%i:%s')
''' ,
#---------------------------------------------UAT TMS Inquiry  Details---------------------------------------
'UAT_Inquiry_BILLS_Details' : '''SELECT  ID  ,  CreationDate  as Creation_Date ,  InquiryBillID  , Value , ProviderDenominationParameterID  as Provider_Denomination_Parameter_ID,
'UAT_V2' as  Source  
FROM STAGING.TMS_UAT_InquiryBillDetails WHERE  CreationDate  BETWEEN STR_TO_DATE('{}', '%Y-%m-%d %H:%i:%s') AND STR_TO_DATE('{}', '%Y-%m-%d %H:%i:%s')  ''' ,
#--------------------------------------------------Regions-----------------------------------------------------
 'REGIONS'  :      '''SELECT * FROM STAGING.ID_PROD_Regions '''     ,
#-------------------------------------------------TMS Inquiry Bills -----------------------------------------------
'Inquiry_Bill'  : '''SELECT
 ID , 
 CreationDate  as Creation_Date ,
 Amount  , 
 Sequence  ,
 ProviderServiceResponseID  as Provider_Service_Response_ID ,
 Mandatory  , 
 MaxAmount  ,
 MinAmount ,
 OpenAmount ,
'GW_V2'as  Source  
FROM STAGING.TMS_PROD_InquiryBills WHERE  CreationDate  BETWEEN STR_TO_DATE('{}', '%Y-%m-%d %H:%i:%s') AND STR_TO_DATE('{}', '%Y-%m-%d %H:%i:%s') ''',
#------------------------------------------------UAT TMS Inquiry Bills --------------------------------------------------
'UAT_Inquiry_Bill': '''SELECT
 ID , 
 CreationDate  as Creation_Date ,
 Amount  , 
 Sequence  ,
 ProviderServiceResponseID  as Provider_Service_Response_ID ,
 Mandatory  , 
 MaxAmount  ,
 MinAmount ,
 OpenAmount  ,
'UAT_V2' as  Source 
FROM STAGING.TMS_UAT_InquiryBills WHERE  CreationDate  BETWEEN STR_TO_DATE('{}', '%Y-%m-%d %H:%i:%s') AND STR_TO_DATE('{}', '%Y-%m-%d %H:%i:%s')  ''' ,
#------------------------------------------------ TMS LOGS -----------------------------------------------------------

#-----------------------------------------------TMS_UAT_LOGS----------------------------------------------------------

#----------------------------------------------TMS Provider Service Requests ------------------------------------------
'ProviderServiceRequests' : ''' SELECT DISTINCT P.ID,P.CreationDate,P.UpdateDate,P.RequestTypeID,P.ProviderServiceRequestStatusID,P.Brn,P.DenominationID,P.CreatedBy,P.UpdatedBy,P.BillingAccount,'GW_V2' as  Source   FROM STAGING.TMS_PROD_ProviderServiceRequests P
  WHERE  CreationDate  BETWEEN STR_TO_DATE('{}', '%Y-%m-%d %H:%i:%s') AND STR_TO_DATE('{}', '%Y-%m-%d %H:%i:%s') ''' ,
#---------------------------------------------- UAT Provider services requests--------------------------------------
'UAT_ProviderServiceRequests': '''SELECT P.* , 'UAT_V2' as  Source  FROM  STAGING.TMS_UAT_ProviderServiceRequests P  WHERE  CreationDate  BETWEEN STR_TO_DATE('{}', '%Y-%m-%d %H:%i:%s') AND STR_TO_DATE('{}', '%Y-%m-%d %H:%i:%s')  ''' , 
#-----------------------------------------------TMS ProviderService Response---------------------------------------
'ProviderServiceResponse' : '''SELECT DISTINCT  ID  ,  CreationDate as  Creation_Date  , ProviderServiceRequestID  as  Provider_Service_Request_ID  ,  TotalAmount   , 'GW_V2' as  Source 
FROM STAGING.TMS_PROD_ProviderServiceResponses WHERE  CreationDate  BETWEEN STR_TO_DATE('{}', '%Y-%m-%d %H:%i:%s') AND STR_TO_DATE('{}', '%Y-%m-%d %H:%i:%s') ''' , 
#-----------------------------------------------UAT Provider service response---------------------------------------
'UAT_ProviderServiceResponse': '''SELECT  ID  ,  CreationDate as  Creation_Date  , ProviderServiceRequestID  as  Provider_Service_Request_ID  ,  TotalAmount  ,'UAT_V2' as  Source 
FROM STAGING.TMS_UAT_ProviderServiceResponses WHERE  CreationDate  BETWEEN STR_TO_DATE('{}', '%Y-%m-%d %H:%i:%s') AND STR_TO_DATE('{}', '%Y-%m-%d %H:%i:%s')  ''' ,
#-----------------------------------------------TMS Provider ServiceResponseParams ----------------------------------
'ProviderServiceResponseParams' : '''SELECT  ID  ,  CreationDate  as  Creation_Date  ,  ProviderServiceResponseID  as Provider_Service_Response_ID , Value  ,  ParameterID  as  Parameter_ID  ,  Brn ,'GW_V2'as  Source 
FROM STAGING.TMS_PROD_ProviderServiceResponseParams WHERE  CreationDate  BETWEEN STR_TO_DATE('{}', '%Y-%m-%d %H:%i:%s') AND STR_TO_DATE('{}', '%Y-%m-%d %H:%i:%s') ''' , 
#-----------------------------------------------UAT Provicer service Response para----------------------------------
'UAT_ProviderServiceResponseParams': '''SELECT  ID  ,  CreationDate  as  Creation_Date  ,  ProviderServiceResponseID  as Provider_Service_Response_ID , Value  ,  ParameterID  as  Parameter_ID  ,  Brn ,'UAT_V2' as  Source 
FROM STAGING.TMS_UAT_ProviderServiceResponseParams WHERE  CreationDate  BETWEEN STR_TO_DATE('{}', '%Y-%m-%d %H:%i:%s') AND STR_TO_DATE('{}', '%Y-%m-%d %H:%i:%s')  ''' , 
#-----------------------------------------------TMS ProviderServiceRequestParams ------------------------------------
'ProviderServiceRequestParams' : '''SELECT  ID  ,  CreationDate  , ParameterID ,'GW_V2' as  Source
FROM STAGING.TMS_PROD_ProviderDenominationParameters WHERE  CreationDate  BETWEEN STR_TO_DATE('{}', '%Y-%m-%d %H:%i:%s') AND STR_TO_DATE('{}', '%Y-%m-%d %H:%i:%s') ''',
#-----------------------------------------------UAT Provider Service Request params------------------------------------
'UAT_ProviderServiceRequestParams': '''SELECT  ID  ,  CreationDate  , ParameterID ,'UAT_V2' as  Source FROM STAGING.TMS_UAT_ProviderServiceResponseParams WHERE  CreationDate  BETWEEN STR_TO_DATE('{}', '%Y-%m-%d %H:%i:%s') AND STR_TO_DATE('{}', '%Y-%m-%d %H:%i:%s')  ''' , 
#---------------------------------------------- TMS Provider Transaction Response ---------------------------------------
'ProviderTransactionResponses' : '''SELECT  ID  ,  CreationDate  as  Creation_Date  ,  TransactionID  as  Transaction_ID  , 
 ProviderTransactionID as  Provider_Transaction_ID  ,  RequestID  ,'GW_V2' as  Source 
FROM STAGING.TMS_PROD_ProviderTransactionResponses WHERE  CreationDate  BETWEEN STR_TO_DATE('{}', '%Y-%m-%d %H:%i:%s') AND STR_TO_DATE('{}', '%Y-%m-%d %H:%i:%s') ''' ,
#---------------------------------------------- UAT Provider Transaction Response-------------------------------
'UAT_ProviderTransactionResponses': '''SELECT  ID  ,  CreationDate  as  Creation_Date  ,  TransactionID  as  Transaction_ID  , 
 ProviderTransactionID as  Provider_Transaction_ID  ,  RequestID  ,'UAT_V2' as  Source 
FROM STAGING.TMS_UAT_ProviderTransactionResponses WHERE  CreationDate  BETWEEN STR_TO_DATE('{}', '%Y-%m-%d %H:%i:%s') AND STR_TO_DATE('{}', '%Y-%m-%d %H:%i:%s')  ''' ,
#---------------------------------------------TMS Provider Denomination -------------------------------------------
'PROVIDERDENOMINATIONPARAMETERS' : '''SELECT * FROM STAGING.TMS_PROD_ProviderDenominationParameters ''' ,
#------------------------------------------- TMS ReceiptBodyParams -----------------------------------------------
'ReceiptBodyParams' : '''SELECT  ID  ,  CreationDate  as  Creation_Date  ,  ProviderServiceRequestID  as  Provider_Service_Request_ID  ,  Value  , 
 TransactionID  as  Transaction_ID  , ProviderDenominationParameterID  as  Provider_Denomination_Parameter_ID ,'GW_V2' as  Source   
FROM STAGING.TMS_PROD_ReceiptBodyParams WHERE  CreationDate  BETWEEN STR_TO_DATE('{}', '%Y-%m-%d %H:%i:%s') AND STR_TO_DATE('{}', '%Y-%m-%d %H:%i:%s') ''' ,
#--------------------------------------------UAT ReceiptBody params----------------------------------------
'UAT_ReceiptBodyParams': '''SELECT  ID  ,  CreationDate  as  Creation_Date  ,  ProviderServiceRequestID  as  Provider_Service_Request_ID  ,  Value  , 
 TransactionID  as  Transaction_ID  , ProviderDenominationParameterID  as  Provider_Denomination_Parameter_ID  ,'UAT_V2' as  Source  
FROM STAGING.TMS_UAT_ReceiptBodyParams WHERE  CreationDate  BETWEEN STR_TO_DATE('{}', '%Y-%m-%d %H:%i:%s') AND STR_TO_DATE('{}', '%Y-%m-%d %H:%i:%s')   ''' ,
#-------------------------------------------------TMS TRANSFERCATEGORIES------------------------------------
'TRANSFERCATEGORIES' : '''SELECT  ID  ,  CreationDate  as  Creation_Date  ,  CategoryName  ,  CatgoryNameAr  
FROM  STAGING.TMS_PROD_TransferCategories WHERE  CreationDate  BETWEEN STR_TO_DATE('{}', '%Y-%m-%d %H:%i:%s') AND STR_TO_DATE('{}', '%Y-%m-%d %H:%i:%s') ''' ,
#----------------------------------------------------TMS TransferSubCategories------------------------------------
'TRANSFERSUBCATEGORIES' : '''SELECT  ID  ,  CreationDate  as  Creation_Date  ,  SubCategoryName  as  Sub_Category_Name  , SubCategoryNameAr  as  Sub_Category_NameAr ,
 TransferCategoryID  as  Transfer_Category_ID  ,  DenominationID  ,  AccountID  
FROM  STAGING.TMS_PROD_TransferSubCategories WHERE  CreationDate  BETWEEN STR_TO_DATE('{}', '%Y-%m-%d %H:%i:%s') AND STR_TO_DATE('{}', '%Y-%m-%d %H:%i:%s') ''' ,
#--------------------------------------------------------TMS TransferRequests--------------------------
'TransferRequests'  : ''' SELECT t.* , 'GW_V2' as Source FROM STAGING.TMS_PROD_TransferRequests t WHERE  t.CreationDate BETWEEN STR_TO_DATE('{}', '%Y-%m-%d %H:%i:%s') AND STR_TO_DATE('{}', '%Y-%m-%d %H:%i:%s')  '''   ,
#---------------------------------------------------------UAT TMS TransferRequests----------------------
'UAT_TransferRequests': ''' SELECT t.*,'UAT_V2' as Source FROM STAGING.TMS_UAT_TransferRequests t WHERE t.CreationDate BETWEEN STR_TO_DATE('{}', '%Y-%m-%d %H:%i:%s') AND STR_TO_DATE('{}', '%Y-%m-%d %H:%i:%s')  ''' ,
# ---------------------------------------------------SOF ACCOUNTSERVICEAVAILABLEBALANCES----------------------------------------------
'ACCOUNT_SERVICE_AVAILABLE_BALANCES' : '''SELECT *
FROM STAGING.SOF_PROD_ACCOUNTSERVICEAVAILABLEBALANCES ''',
#-----------------------------------------------SOF AccountServiceBalances-----------------------------------------------------------
'ACCOUNTSERVICEBALANCES' : '''SELECT *
FROM STAGING.SOF_PROD_ACCOUNTSERVICEBALANCES ''',
#----------------------------------------------SOF BalanceHistories--------------------------------------------------------
 'BalanceHistories': '''
        SELECT BH.ID,BH.CreationDate,BH.UpdateDate,BH.TransactionID,BH.BalanceBefore,BH.AccountID,BH.BalanceTypeID,BH.TotalBalance,
        BH.RequestID,BH.AvaialableBalanceAfter,BH.ActionID, 'GW_V2' as Source
        FROM STAGING.SOF_PROD_BalanceHistories BH
        JOIN STAGING.TMS_PROD_Requests R ON BH.RequestID = R.ID 
        WHERE BH.CreationDate BETWEEN STR_TO_DATE('{}', '%Y-%m-%d %H:%i:%s') AND STR_TO_DATE('{}', '%Y-%m-%d %H:%i:%s')
    ''',
#------------------------------------------------UAT Balance history--------------------------------------------------------
'UAT_BalanceHistories': '''
        SELECT BH.*, 'UAT_V2' as Source
        FROM STAGING.SOF_UAT_BalanceHistories BH
        JOIN STAGING.TMS_UAT_Requests R ON BH.RequestID = R.ID 
        WHERE BH.CreationDate BETWEEN STR_TO_DATE('{}', '%Y-%m-%d %H:%i:%s') AND STR_TO_DATE('{}', '%Y-%m-%d %H:%i:%s')
    ''',
#---------------------------------------------SOF HoldBalances-------------------------------------------------------------
'HoldBalances': '''
        SELECT  H.ID,H.CreationDate,H.UpdateDate,H.AccountID,H.RequestID,H.Amount,H.SourceID,H.Status,H.AvailableBalanceBefore,H.BalanceTypeID,H.ConfirmedAmount,H.CreditLimitBefore,'GW_V2' as Source
        FROM STAGING.SOF_PROD_HoldBalances H  
        JOIN STAGING.TMS_PROD_Requests R ON H.RequestID = R.ID
        WHERE H.CreationDate BETWEEN STR_TO_DATE('{}', '%Y-%m-%d %H:%i:%s') AND STR_TO_DATE('{}', '%Y-%m-%d %H:%i:%s')
    ''',
#---------------------------------------------UAT SOF HOLDBALANCEs-----------------------------------------------------
'UAT_HoldBalances': '''
        SELECT H.*, 'UAT_V2' as Source
        FROM STAGING.SOF_UAT_HoldBalances H  
        JOIN STAGING.TMS_UAT_Requests R ON H.RequestID = R.ID 
        WHERE H.CreationDate BETWEEN STR_TO_DATE('{}', '%Y-%m-%d %H:%i:%s') AND STR_TO_DATE('{}', '%Y-%m-%d %H:%i:%s')
    ''',
#-----------------------------------------------------------call back we -------------------------------------------------
'CALLBACKWE' : '''SELECT * FROM STAGING.GW_PROD_CallBackWE WHERE  CallBackDate  BETWEEN STR_TO_DATE('{}', '%Y-%m-%d %H:%i:%s') AND STR_TO_DATE('{}', '%Y-%m-%d %H:%i:%s') ''',
#-----------------------------------------GW Request Provider Details -------------------------------------
'Request_Provider_Details': '''
        SELECT  P.ID, P.RequestID, P.ProviderTransactionId, P.ProviderFees, R.ServiceDenominationID as Service_Denomination_ID, 'PROD' as Source
        FROM STAGING.GW_PROD_RequestProviderDetails P
        JOIN STAGING.GW_PROD_REQUESTS R ON R.ID = P.RequestID
        where RequestDate BETWEEN STR_TO_DATE('{}', '%Y-%m-%d %H:%i:%s') AND STR_TO_DATE('{}', '%Y-%m-%d %H:%i:%s')
    ''',
#------------------------------------------UAT REQUEST PROVIDER DETAILS------------------------------------
'UAT_Request_Provider_Details': '''
        SELECT P.ID, P.RequestID, P.ProviderTransactionId, P.ProviderFees, R.ServiceDenominationID as Service_Denomination_ID, 'UAT' as Source
        FROM STAGING.GW_UAT_RequestProviderDetails P
        JOIN STAGING.GW_UAT_REQUESTS R ON R.ID = P.RequestID
        WHERE RequestDate BETWEEN STR_TO_DATE('{}', '%Y-%m-%d %H:%i:%s') AND STR_TO_DATE('{}', '%Y-%m-%d %H:%i:%s')
    ''',
#-------------------------------------------Account  ACCOUNT_RELATION_MAPPING-------------------------
'ACCOUNT_RELATION_MAPPING': '''
        SELECT ID, AccountID, ParentID, InsertedDateTime
        FROM (
            SELECT A.*, ROW_NUMBER() OVER (PARTITION BY AccountID ORDER BY InsertedDateTime) AS RN
            FROM STAGING.GW_PROD_AccountRelationMapping A
        ) AS temp
        WHERE RN = 1
    ''',
#-------------------------------------------ACCOUNT_RELATION_MAPPING_HISTORY-------------------------
'ACCOUNT_RELATION_MAPPING_HISTORY' : ''' SELECT * FROM STAGING.GW_PROD_AccountRelationMappingHistory ''',
#------------------------------------------FEES -------------------------------------------------
'FEES': '''SELECT * FROM STAGING.TMS_PROD_Fees WHERE CreationDate BETWEEN STR_TO_DATE('{}', '%Y-%m-%d %H:%i:%s') AND STR_TO_DATE('{}', '%Y-%m-%d %H:%i:%s') '''  ,
#-------------------------------------------Account_FEES----------------------------------------
'ACCOUNT_FEES':'''SELECT * FROM STAGING.TMS_PROD_AccountFees''' ,
#------------------------------------Electricity_log------------------------
'Electricity_log' :     '''SELECT  Id , AddedTime , amount , ProviderTransactionId , InvId , transactions_types_id  AS  Transactions_types_id , Code , RequestIdInquiry , ServicesId  AS  services_id FROM STAGING.GW_PROD_Electricity_log WHERE  AddedTime  BETWEEN STR_TO_DATE('{}', '%Y-%m-%d %H:%i:%s') AND STR_TO_DATE('{}', '%Y-%m-%d %H:%i:%s') '''    ,
#---------------------------------PengingPaymentCard---------------------------
'PengingPaymentCard':     '''SELECT  ID , RequestID , CardTypeID , PengingPaymentCardStatusID , InsertedDate , UpdatedDate 
                          FROM STAGING.GW_PROD_PengingPaymentCard 
                          WHERE  InsertedDate  BETWEEN STR_TO_DATE('{}', '%Y-%m-%d %H:%i:%s') AND STR_TO_DATE('{}', '%Y-%m-%d %H:%i:%s') '''  ,
#---------------------------------------EBC_AcquirerBankFees-----------------------------------
'EBC_AcquirerBankFees'  :    '''SELECT * FROM STAGING.ReportsDB_AcquirerBankFees'''   ,
#--------------------------------------EBC_BAF------------------------------------
'EBC_BAF'  :    '''SELECT * FROM STAGING.ReportsDB_BAF'''   ,
#-------------------------------------EBC_BillerShare-------------------------------------
'EBC_BillerShare'  :    '''SELECT * FROM STAGING.ReportsDB_BillerShare'''   ,
#--------------------------------------EBC_MDR------------------------------------
'EBC_MDR'  :    '''SELECT * FROM STAGING.ReportsDB_MDR'''   ,
#----------------------------------------EBC_ParentGroups----------------------------------
'EBC_ParentGroups'  :    '''SELECT * FROM STAGING.ReportsDB_ParentGroups'''   ,
#-----------------------------------------EBC_ServiceType---------------------------------
'EBC_ServiceType' :    '''SELECT * FROM STAGING.ReportsDB_ServiceType'''   ,
#-----------------------------------------GW_PROD_Reasons------------------------
'Reasons' :   '''SELECT * FROM STAGING.GW_PROD_Reasons'''   ,
#-----------------------------------------GW_PROD_VersionTracking------------------------
'VersionTracking' :   '''SELECT * FROM GW_PROD_VersionTracking WHERE  CreationDate  BETWEEN STR_TO_DATE('{}', '%Y-%m-%d %H:%i:%s') AND STR_TO_DATE('{}', '%Y-%m-%d %H:%i:%s') '''    ,
#-----------------------------------------GW_PROD_ResetPasswordHistory------------------------
'ResetPasswordHistory' :   '''SELECT * FROM STAGING.GW_PROD_ResetPasswordHistory'''   ,
#-----------------------------------------GW_PROD_channel_categories------------------------
'channel_categories' :    '''select  ID , Name , ArName  from STAGING.GW_PROD_ChannelCategories'''   ,
#-----------------------------------------GW_PROD_channel_owners------------------------
'channel_owners' :    '''select  ID , Name , ArName  from STAGING.GW_PROD_ChannelOwners'''   ,
#-----------------------------------------GW_PROD_balance_credit------------------------
'balance_credit' :    '''select  Id , CenterId , Amount , Type , Descrp , AddedTime , UserId , Before , Invoice_cnt 
        , Invoice_income , Charge_cnt , Charge_income , Voucher_cnt , Voucher_income ,
         Total_Amount , Invoice , Charge , Voucher , TotalPoint  
from STAGING.GW_PROD_Balance_Credit
WHERE  AddedTime  BETWEEN STR_TO_DATE('{}', '%Y-%m-%d %H:%i:%s') AND STR_TO_DATE('{}', '%Y-%m-%d %H:%i:%s') '''   ,


'PROVIDER_SERVICE_REQUESTS':     '''SELECT DISTINCT id,  RequestTypeID ,  ProviderServiceRequeststatusID ,  Brn ,  DenominationID ,  CreationDate ,  CreatedBy ,  UpdateDate ,  UpdatedBy ,     BillingAccount , 'GW' AS  SourceSystem  FROM STAGING.GW_PROD_PROVIDERSERVICEREQUESTS
     WHERE CreationDate  BETWEEN STR_TO_DATE('{}', '%Y-%m-%d %H:%i:%s') AND STR_TO_DATE('{}', '%Y-%m-%d %H:%i:%s') '''       ,
     
'UAT_PROVIDER_SERVICE_REQUESTS':     '''SELECT id,  RequestTypeID ,  ProviderServiceRequeststatusID ,  Brn ,  DenominationID ,  CreationDate ,  CreatedBy ,  UpdateDate ,  UpdatedBy ,     BillingAccount , 'UAT' AS  SourceSystem  FROM STAGING.GW_UAT_PROVIDERSERVICEREQUESTS
     WHERE CreationDate  BETWEEN STR_TO_DATE('{}', '%Y-%m-%d %H:%i:%s') AND STR_TO_DATE('{}', '%Y-%m-%d %H:%i:%s') '''     ,
     
'TMS_PROVIDER_SERVICE_REQUESTS':     '''SELECT DISTINCT id,  RequestTypeID ,  ProviderServiceRequeststatusID ,  Brn ,  DenominationID ,  CreationDate ,  CreatedBy ,  UpdateDate ,  UpdatedBy ,     BillingAccount , 'GW_V2' AS  SourceSystem  FROM STAGING.TMS_PROD_ProviderServiceRequests
     WHERE CreationDate  BETWEEN STR_TO_DATE('{}', '%Y-%m-%d %H:%i:%s') AND STR_TO_DATE('{}', '%Y-%m-%d %H:%i:%s') '''       ,
     
'UAT_TMS_PROVIDER_SERVICE_REQUESTS':     '''SELECT id,  RequestTypeID ,  ProviderServiceRequeststatusID ,  Brn ,  DenominationID ,  CreationDate ,  CreatedBy ,  UpdateDate ,  UpdatedBy ,     BillingAccount , 'UAT_V2' AS  SourceSystem  FROM STAGING.TMS_UAT_ProviderServiceRequests'''     
}
#******************************************************************************
DimensionTables = ["ACCOUNTS", "SERVICE_PROVIDER", "PARENT_GROUPS", 
                   'SERVICE_GROUPS', 'SERVICE','SERVICES','ACCOUNT_OWNERS',
                    'DENOMINATIONS', 'TRANSACTIONS_TYPES','PROVIDER',
                   'DENOMINATION_SERVICE_PROVIDER', 'PLACE','CHANNELS', 'ACCOUNT_TYPE', 
                   'SERVICE_FIELDS', 'REQUEST_STATUS', 'CHANNEL_TYPES', 
                   'ACCOUNT_CHANNELS', 'CHANNEL_IDENTIFIERS', 
                   'PAYMENT_TYPE', 'ACCOUNT_LOCATIONS', 'Account_Type_Profile','REGIONS',
                    'GOVERNORATES',  "PROVIDERDENOMINATIONPARAMETERS",
                   "TRANSFERCATEGORIES", "TRANSFERSUBCATEGORIES",'ACCOUNT_FEES' ,
                   "ACCOUNT_SERVICE_AVAILABLE_BALANCES", "ACCOUNTSERVICEBALANCES" , "ACCOUNT_RELATION_MAPPING", "ACCOUNT_RELATION_MAPPING_HISTORY",
                    "EBC_AcquirerBankFees", "EBC_BAF", "EBC_BillerShare", "EBC_MDR", "EBC_ParentGroups", "EBC_ServiceType","Reasons",
                    'channel_categories','channel_owners','ResetPasswordHistory' ]
#-------------------------------------------------------
#--- TO DO  ##########################################
def Handling_UPSERT(ODS_Dict ):
   pass
#---------------------------------------------Global Varibles -------------------
new_date = int(datetime.strftime(datetime.now(), '%Y%m%d'))
error_messages = []
#-----------------------Inserting Date to string ------------------------------
def Adding_Date(Dict , date_From , date_To):
    for key, value in Dict.items():
         Dict[key] = value.format(date_From , date_To)
    return Dict
#-----------------STAGING Reading------------------------------------------

def reading_STG_DORIS(user, pass_, schema_name, ODS_Queries):
    """
    Reads dimension tables from Doris.

    Parameters:
    - user: str, username for Doris database
    - pass_: str, password for Doris database
    - schema_name: str, name of the schema
    - ODS_Queries: dict, a dictionary with table names as keys and corresponding SQL queries as values

    Returns:
    - dict: a dictionary with keys as table names and values as corresponding DataFrames
    """
    
    # Dictionary to hold DataFrames
    Dict = {}
    
    # JDBC URL for Doris
    jdbc_url = f"jdbc:mysql://10.90.6.135:9030/{schema_name}"
    
    # Iterating over each key-value pair in the provided dictionary
    for table_name, query in ODS_Queries.items():
        try:
            print('Table to be read is: {}'.format(table_name))
            
            # Reading the table into a DataFrame using the SQL query
            df = spark.read \
                .format("jdbc") \
                .option("url", jdbc_url) \
                .option("driver", "com.mysql.cj.jdbc.Driver") \
                .option("dbtable", f"({query}) as t") \
                .option("user", user) \
                .option("password", pass_) \
                .load()
            
            # Storing the DataFrame in the dictionary
            Dict[table_name] = df
            
            # Print success message
            print('{} loaded successfully'.format(table_name))
        
        except Exception as e:
            print(f"Error loading table {table_name}: {str(e)}")
    
    return Dict
 
 #--------------------------------------------------------------------------
Dict = {}
def Reading_STAGING(o_user , o_pass , ODS_Queries):
    for key , value in  ODS_Queries.items():
            dfw = spark.read.format('jdbc').option('driver' ,'oracle.jdbc.driver.OracleDriver' )\
            .option('url' ,'jdbc:oracle:thin:@10.90.6.130:1521:dwhdb')\
            .option('user' ,o_user)\
            .option('password' , o_pass)\
            .option('query',value).load()
            print('Table {} has been read successfully '.format(key))
            Dict[key] = dfw
    return Dict
#-------------------Readig from ODS----------------------------------------
def Reading_ODS(o_user , o_pass , DimensionTables): #reading from STAGING the dimension tables.
    Dim_dict = {}
    for value in  DimensionTables:
            table_name = 'ODS'+'.'+ value
            dfw = spark.read.format('jdbc').option('driver' ,'oracle.jdbc.driver.OracleDriver' )\
            .option('url' ,'jdbc:oracle:thin:@10.90.6.130:1521:dwhdb')\
            .option('user' ,o_user)\
            .option('password' , o_pass)\
            .option('dbtable',table_name).load()
            print('Table {} has been read successfully '.format(value))
            Dim_dict[value] = dfw
    return Dim_dict
#------------------------------------------------------------------------
'''
def write_updatedTo_ODS(user , pass_ , shcema_name ,diff_rows ,key) : #writting the only updated row in the dimension table.
        table_name = shcema_name+'.'+ key
        diff_rows.write.format('jdbc').option('driver' ,'oracle.jdbc.driver.OracleDriver' )\
                .option('url' ,'jdbc:oracle:thin:@10.90.6.130:1521:dwhdb')\
                .option('user' ,user)\
                .option('password' , pass_)\
                .mode('append')\
                .option('IsolationLevel','READ_COMMITTED')\
                .option('dbtable',table_name).save()
        print(table_name+' '+'--------loaded successfully')
#----------------------------------------------------------------------
 
def writting_changing_DIM(Dim_dict , Dict , DimensionTables ,schema_name ,user , pass_) :
    special_tables = ['DENOMINATION_SERVICE_PROVIDER' , 'SERVICE_PROVIDER']
    for key in DimensionTables :
      if  key  not in special_tables:
        ID1 = list( Dim_dict[key].columns )[0]
        ID2 = list( Dict[key].columns )[0]
        subtracted_df = Dict[key].join(Dim_dict[key] ,Dim_dict[key][ID1]==Dict[key][ID2] , how='left_anti'  )
      else:
          if  key == 'DENOMINATION_SERVICE_PROVIDER':
             subtracted_df =  Dict[key].join(Dim_dict[key] ,(Dim_dict[key]['Denomination_ID']==Dict[key]['DENOMINATION_ID']) & (Dim_dict[key]['Service_Provider_ID']== Dict[key]['Service_Provider_ID']) & (Dim_dict[key]['Source']==Dict[key]['Source']) , how='left_anti'  )
          else:
             subtracted_df = Dict[key].join(Dim_dict[key] ,(Dim_dict[key]['ID']==Dict[key]['ID']) & (Dim_dict[key]['Source']==Dict[key]['Source']  ) &(Dim_dict[key]['Name']==Dict[key]['Name'] ) , how='left_anti'  )

      if subtracted_df.count()!=0:
        try:
           print('Rows to be added \n Table To be updated {} \n'.format(key) )
           write_updatedTo_ODS(user , pass_ , schema_name , subtracted_df ,key)
        except Exception as e:
            # Log the exception in the "ODS_ETL_EXCEPTION" table
            error_messages.append((datetime.now(), ODS_Queries[key], str(e) ,str(key) ))
            print('Exception occurred for {}: {}'.format(table_name, str(e)))
      else :
            print('There"s no change in {}'.format(key),'Table')
      del Dict[key]
    return Dict
'''

import jaydebeapi
from pyspark.sql.types import *
from datetime import datetime

def check_and_create_table(user, pass_, schema_name, table_name, sample_df):
    try:
        # Establish connection to Doris
        conn = jaydebeapi.connect(
            'com.mysql.cj.jdbc.Driver',
            f'jdbc:mysql://10.90.6.135:9030/information_schema',
            [user, pass_],
            '/home/Application/Apache_Spark/ETL/Doris/mysql-connector-j-8.2.0.jar'
        )
        curs = conn.cursor()
        
        # Check if the table exists
        curs.execute(f"SELECT COUNT(*) FROM tables WHERE table_schema = '{schema_name}' AND table_name = '{table_name.split('.')[-1]}'") 
        exists = curs.fetchone()[0]
        
        if not exists:
            # Column type mapping
            def map_dtype(dtype):
                if isinstance(dtype, (FloatType, DoubleType,DecimalType)):
                    return 'DECIMAL(38,10)'
                elif isinstance(dtype, StringType):
                    return 'VARCHAR' 
                elif isinstance(dtype, LongType):
                    return 'BIGINT' 
                elif isinstance(dtype, TimestampType):
                    return 'DATETIME'  # Map Spark TimestampType to MySQL DATETIME
                elif isinstance(dtype, IntegerType):
                    return 'INT'  # Map Spark IntegerType to MySQL INT
                else:
                    return 'VARCHAR'  # Default to VARCHAR for other types
            
            # Get column names and their mapped types
            columns = [f"{col_name} {map_dtype(sample_df.schema[col_name].dataType)}" for col_name in sample_df.columns]
            columns_str = ', '.join(columns)
            
            # Create table query
            create_query = f"""
                CREATE TABLE {table_name} (
                    {columns_str}
                )
                DISTRIBUTED BY HASH({sample_df.columns[0]}) BUCKETS 10
                PROPERTIES('replication_num' = '1')
            """
            
            # Execute create table query
            curs.execute(create_query)
            print(f"Table {table_name} created successfully.")
        
        # Close cursor and connection
        curs.close()
        conn.close()
    
    except Exception as e:
        print(f"Error while checking or creating table {table_name}: {e}")
        if 'curs' in locals():
            curs.close()
        if 'conn' in locals():
            conn.close()
# Usage example:
# sample_df = ...  # Your Spark DataFrame
# check_and_create_table('user', 'password', 'schema_name', 'table_name', sample_df)

            
#---------------------------------------------------------------------------------------

        
#------------------------------------------------------------------------------------------------------
        

def writting_DIM(user, pass_, schema_name, Dict):
    #TABLES = ['ACCOUNTS']  # List of tables to write, you can add more table names to this list
    # Assuming DimensionTables are the keys in Dict
    for key in DimensionTables:
        print('Table to be written is: {}'.format(key))
        table_name = schema_name + '.' + key
        sample_df = Dict[key].limit(1)
        check_and_create_table(user, pass_, schema_name, table_name, sample_df)
        Dict[key].write.format("doris") \
             .mode("overwrite") \
             .option("doris.table.identifier", table_name) \
             .option("doris.fenodes", "10.90.6.135:8030") \
             .option("doris.query.port", "9030") \
             .option("user", user) \
             .option("password", pass_) \
             .option("doris.write.replicated", "1") \
             .option("delimiter", "`") \
             .option("sep", "\t") \
             .option("failOnMissingOrIncompatibleColumn", True) \
             .save()
        print(table_name + ' loaded successfully')
        del Dict[key]
    return Dict
        
#-----------------------------------------------------------------------------------------------------


def write_to_oracle(user, pass_, schema_name, Dict):
    #tables = ['ELECTRONIC_CHARGE_VOUCHER']
    excluded_tables = ['GW_PROD_LOGS', 'GW_UAT_LOGS', 'TMS_PROD_Logs', 'TMS_UAT_Logs']
    for key, value in Dict.items():
 
        if key not in excluded_tables :
           print('Table to be written is: {}'.format(key))
           table_name = schema_name + '.' + key
           sample_df = Dict[key].limit(1)
           check_and_create_table(user, pass_, schema_name, table_name, sample_df)
           table_name = schema_name + '.' + key
           Dict[key].write.format("doris") \
               .mode("append") \
               .option("doris.table.identifier", table_name) \
               .option("doris.fenodes", "10.90.6.135:8030") \
               .option("doris.query.port", "9030") \
               .option("user", user) \
               .option("password", pass_) \
               .option("doris.write.replicated", "1") \
               .option("delimiter", "`") \
               .option("sep", "\t") \
               .option("failOnMissingOrIncompatibleColumn", True) \
               .save()
           print(table_name+' '+'loaded successfully')
 


#************************************************* Transformations ***********************************************************

def Requests_Transformation(ODS_Dict) :
    ODS_Dict['REQUESTS'] = ODS_Dict['REQUESTS'].repartition(200)
    ODS_Dict['REQUESTS'] = ODS_Dict['REQUESTS'].union(ODS_Dict['UAT_REQUESTS'])
    ODS_Dict['TMS_REQUESTS'] = ODS_Dict['TMS_REQUESTS'].union(ODS_Dict['UAT_TMS_REQUESTS'])
    ODS_Dict['REQUESTS'] = ODS_Dict['REQUESTS'].union(ODS_Dict['TMS_REQUESTS'])
    

    #------------------------------------------------------------------------------------------------------------
    ODS_Dict['REQUESTS'] = ODS_Dict['REQUESTS'].withColumn('RESPONSE_DATE',when(col('RESPONSE_DATE').isNull(),  col('REQUEST_DATE')).otherwise(col('RESPONSE_DATE')))\
                                               .withColumn('PROVIDER_SERVICE_REQUEST_ID',when(col('PROVIDER_SERVICE_REQUEST_ID').isNull(),  0).otherwise(col('PROVIDER_SERVICE_REQUEST_ID')))\
                                               .withColumn('Billing_Account',when(col('Billing_Account').isNull(),  'Unknown').otherwise(col('BILLING_ACCOUNT')))\
                                               .withColumn('CHANNEL_ID',when(col('CHANNEL_ID').isNull(),  'Unkown').otherwise(col('CHANNEL_ID')))\
                                               .withColumn('PROVIDER_SERVICE_REQUEST_ID',when(col('PROVIDER_SERVICE_REQUEST_ID').isNull(),  0).otherwise(col('PROVIDER_SERVICE_REQUEST_ID')))\
                                               .withColumn('CompositeTransactionID',when(col('CompositeTransactionID').isNull(),  'Unkown').otherwise(col('CompositeTransactionID')))\
                                               .withColumn('ID' ,when( (col('SourceSystem')=='UAT') | (col('SourceSystem')=='UAT_V2')   ,  concat( lit( new_date* -1 ) , col('ID') ) ).otherwise(col('ID')))\
                                               .withColumn('UUID',when(col('UUID').isNull(), 'Unknown').otherwise(col('UUID'))) 
    ODS_Dict['REQUESTS'] = ODS_Dict['REQUESTS'].dropDuplicates(['ID'])
    return ODS_Dict 
#**************************************************************************************************************************



#**************************************************************************************************************************

def Invoices_Transformation(ODS_Dict):
    ODS_Dict['INVOICE'] = ODS_Dict['INVOICE'].union(ODS_Dict['UAT_INVOICE'])
    del ODS_Dict['UAT_INVOICE']

    # Transform INVOICE table
    ODS_Dict['INVOICE'] = ODS_Dict['INVOICE'] \
        .withColumn('Invoice_Id_card', when(col('Invoice_Id_card').isNull() | (col('Invoice_Id_card') == 'null'), 0).otherwise(col('Invoice_Id_card'))) \
        .withColumn('service_id', when(col('service_id').isNull() | (col('service_id') == 'null'), 0).otherwise(col('service_id'))) \
        .withColumn('InCome', when(col('InCome').isNull() | (col('InCome') == 'null'), 0).otherwise(col('InCome'))) \
        .withColumn('added_money', when(col('added_money').isNull() | (col('added_money') == 'null'), 0).otherwise(col('added_money'))) \
        .withColumn('Basci_val', when(col('Basci_val').isNull() | (col('Basci_val') == 'null'), 0).otherwise(col('Basci_val'))) \
        .withColumn('Provider_Response', when(col('Provider_Response').isNull() | (col('Provider_Response') == 'null'), 'UNKNOWN').otherwise(col('Provider_Response'))) \
        .withColumn('ProviderTransactionId', when(col('ProviderTransactionId').isNull() | (col('ProviderTransactionId') == 'null'), 0).otherwise(col('ProviderTransactionId'))) \
        .withColumn('ProviderCardTransactionId', when(col('ProviderCardTransactionId').isNull() | (col('ProviderCardTransactionId') == 'null'), 'UNKNOWN').otherwise(col('ProviderCardTransactionId'))) \
        .withColumn('ECardmomknPaymentId', when(col('ECardmomknPaymentId').isNull() | (col('ECardmomknPaymentId') == 'null'), 'UNKNOWN').otherwise(col('ECardmomknPaymentId'))) \
        .withColumn('ClientPhone', when((col('ClientPhone').cast("int").isNotNull()) & (length(col('ClientPhone')) == 11), col('ClientPhone')).otherwise('Unknown')) \
        .withColumn('v_ID', when(col('Source') == 'UAT', concat(lit(new_date * -1), col('Id'))).otherwise(col('Id'))) \
        .withColumn('ClientName', when(col('ClientName').isNull() | (col('ClientName') == 'null'), 'UNKNOWN').otherwise(col('ClientName'))) \
        .withColumn('Code', when(col('Code').isNull() | (col('Code') == 'null'), 'UNKNOWN').otherwise(col('Code')))

    inv_df = ODS_Dict['INVOICE'].select(['Id', 'Basci_val', 'added_money', 'TotalPrice', 'v_ID', 'Source'])
    ODS_Dict['INVOICE'] = ODS_Dict['INVOICE'].withColumn('Id', col('v_ID'))
    ODS_Dict['INVOICE'] = ODS_Dict['INVOICE'].drop('v_ID')
    ODS_Dict['INVOICE'] = ODS_Dict['INVOICE'].dropDuplicates(['Id'])

    # Combine ELECTRONIC_CHARGE and UAT_ELECTRONIC_CHARGE tables
    ODS_Dict['ELECTRONIC_CHARGE'] = ODS_Dict['ELECTRONIC_CHARGE'].union(ODS_Dict['UAT_ELECTRONIC_CHARGE'])
    del ODS_Dict['UAT_ELECTRONIC_CHARGE']
    ODS_Dict['ELECTRONIC_CHARGE'] = ODS_Dict['ELECTRONIC_CHARGE'] \
        .withColumn('Income', when(col('Income').isNull() | (col('Income') == 'null'), 0).otherwise(col('Income'))) \
        .withColumn('E_ID', when(col('Source') == 'UAT', concat(lit(new_date * -1), col('Id'))).otherwise(col('Id')))

    electronic_charge = ODS_Dict['ELECTRONIC_CHARGE'].select(['Id', 'value', 'E_ID'])
    ODS_Dict['ELECTRONIC_CHARGE'] = ODS_Dict['ELECTRONIC_CHARGE'].withColumn('Id', col('E_ID'))
    ODS_Dict['ELECTRONIC_CHARGE'] = ODS_Dict['ELECTRONIC_CHARGE'].drop('E_ID')
    ODS_Dict['ELECTRONIC_CHARGE'] = ODS_Dict['ELECTRONIC_CHARGE'].dropDuplicates(['Id'])

    # Combine ELECTRONIC_CHARGE_VOUCHER and UAT_ELECTRONIC_CHARGE_VOUCHER tables
    ODS_Dict['ELECTRONIC_CHARGE_VOUCHER'] = ODS_Dict['ELECTRONIC_CHARGE_VOUCHER'].union(ODS_Dict['UAT_ELECTRONIC_CHARGE_VOUCHER'])
    del ODS_Dict['UAT_ELECTRONIC_CHARGE_VOUCHER']

    ODS_Dict['ELECTRONIC_CHARGE_VOUCHER'] = ODS_Dict['ELECTRONIC_CHARGE_VOUCHER'] \
        .withColumn('Income', when(col('Income').isNull() | (col('Income') == 'null'), 0).otherwise(col('Income'))) \
		.withColumn('Bulk_Voucher_Serial' ,when( (col('charge_status').substr(1,1)=='{') & (get_json_object(col('charge_status'),"$.vouchers[0].serial" )!=''  )  , get_json_object(col('charge_status'),"$.vouchers[0].serial" ) ).otherwise('Unknown') )\
        .withColumn('Provider_Transaction_ID' ,when(col('charge_status').substr(1,1)=='{',get_json_object(col('charge_status'),"$.vouchers[0].providerTransactionID" )).otherwise(col('masary_trans_id') ))\
        .withColumn('Provider_Transaction_ID',
                   when(col('Provider_Transaction_ID').isNull() | (col('Provider_Transaction_ID') == 'null'), 'Unknown')
                   .otherwise(col('Provider_Transaction_ID')))\
        .withColumn('ev_ID', when(col('Source') == 'UAT', concat(lit(new_date * -1), col('Id'))).otherwise(col('Id')))
    electronicVoucher_df = ODS_Dict['ELECTRONIC_CHARGE_VOUCHER'].select(['Id', 'value', 'ev_ID'])
    ODS_Dict['ELECTRONIC_CHARGE_VOUCHER'] = ODS_Dict['ELECTRONIC_CHARGE_VOUCHER'].withColumn('Id', col('ev_ID'))
    ODS_Dict['ELECTRONIC_CHARGE_VOUCHER'] = ODS_Dict['ELECTRONIC_CHARGE_VOUCHER'].drop('ev_ID', 'charge_status')

    

    # Combine INVOICE_DETAILS and UAT_INVOICE_DETAILS tables
    ODS_Dict['INVOICE_DETAILS'] = ODS_Dict['INVOICE_DETAILS'].union(ODS_Dict['UAT_INVOICE_DETAILS'])
    inv_df = inv_df.withColumnRenamed('Id', 'inv_ID')
    del ODS_Dict['UAT_INVOICE_DETAILS']
    ODS_Dict['INVOICE_DETAILS'] = ODS_Dict['INVOICE_DETAILS'] \
        .withColumn('INVOICE_ID', when(col('INVOICE_ID').isNull() | (col('INVOICE_ID') == 'null'), 0).otherwise(col('INVOICE_ID'))) \
        .withColumn('Value', when(col('Value').isNull() | (col('Value') == 'null'), 'Unknown').otherwise(col('Value'))) \
        .withColumn('ID', when(col('Source') == 'UAT', concat(lit(new_date * -1), col('ID'))).otherwise(col('ID'))) \
        .withColumn('INVOICE_ID', when(col('Source') == 'UAT', concat(lit(new_date * -1), col('INVOICE_ID'))).otherwise(col('INVOICE_ID')))

    ODS_Dict['INVOICE_DETAILS'] = ODS_Dict['INVOICE_DETAILS'].drop('Source')
    ODS_Dict['INVOICE_DETAILS'] = ODS_Dict['INVOICE_DETAILS'].dropDuplicates(['ID'])

    return ODS_Dict, inv_df, electronic_charge, electronicVoucher_df


#**********************************************************************************************************
def Transaction_Transformation(ODS_Dict, inv_df, electronic_charge, electronicVoucher_df):
    # Combine transactions from different sources
    ODS_Dict['TRANSACTIONS'] = ODS_Dict['TRANSACTIONS'].union(ODS_Dict['UAT_TRANSACTIONS'])
    ODS_Dict['TRANSACTIONS'] = ODS_Dict['TRANSACTIONS'].union(ODS_Dict['TMS_TRANSACTIONS'])
    del ODS_Dict['TMS_TRANSACTIONS']
    del ODS_Dict['UAT_TRANSACTIONS']

    # Rename column and perform left join with electronicVoucher_df
    ODS_Dict['TRANSACTIONS'] = ODS_Dict['TRANSACTIONS'].withColumnRenamed("ID", "Trx_id")
    ODS_Dict['TRANSACTIONS'] = ODS_Dict['TRANSACTIONS'].join(electronicVoucher_df, 
                                                             ODS_Dict['TRANSACTIONS']['Invoice_ID'] == electronicVoucher_df['Id'], 
                                                             how='left')

    # Conditional transformations with when() function
    ODS_Dict['TRANSACTIONS'] = ODS_Dict['TRANSACTIONS'].withColumn('Account_ID_From', 
                                        when((col('Account_ID_From').isNull() | (col('Account_ID_From') == "null")) & col('Original_Trx').isNotNull(), 26457).otherwise(col('Account_ID_From'))) \
                                                       .withColumn('Original_Trx', when(col('Original_Trx').isNull() | (col('Original_Trx') == "null"), 0).otherwise(col('Original_Trx'))) \
                                                       .withColumn('Account_ID_To', when(col('Account_ID_To').isNull() | (col('Account_ID_To') == "null"), -1).otherwise(col('Account_ID_To'))) \
                                                       .withColumn('Original_Amount', when(col('Original_Amount').isNull() | (col('Original_Amount') == "null"), col('value')).otherwise(col('Original_Amount'))) \
                                                       .withColumn('Invoice_ID', when(col('Invoice_ID').isNull() | (col('Invoice_ID') == "null"), 0).otherwise(col('Invoice_ID'))) \
                                                       .withColumn('T_Inv_ID', when((col('Invoice_ID') == col('Id')), col('ev_ID')).otherwise(col('Invoice_ID'))) \
                                                       .withColumn('Request_ID', when(col('Request_ID').isNull() | (col('Request_ID') == "null"), 0).otherwise(col('Request_ID'))) \
                                                       .withColumn('Is_Reversed', when(col('Is_Reversed') == "true", 1).otherwise(0))

    # Drop unnecessary columns
    ODS_Dict['TRANSACTIONS'] = ODS_Dict['TRANSACTIONS'].drop('Id', 'value', 'ev_ID')

    # Perform left join with electronic_charge
    ODS_Dict['TRANSACTIONS'] = ODS_Dict['TRANSACTIONS'].join(electronic_charge, 
                                                             ODS_Dict['TRANSACTIONS']['Invoice_ID'] == electronic_charge['Id'], 
                                                             how='left')

    # More conditional transformations
    ODS_Dict['TRANSACTIONS'] = ODS_Dict['TRANSACTIONS'].withColumn('Original_Amount', when(col('Original_Amount').isNull() | (col('Original_Amount') == "null"), col('value')).otherwise(col('Original_Amount'))) \
                                                       .withColumn('T_Inv_ID', when((col('Invoice_ID') == col('Id')), col('E_ID')).otherwise(col('T_Inv_ID'))) \
                                                       .withColumn('Is_Reversed', when(col('Is_Reversed') == "true", 1).otherwise(0))

    # Drop unnecessary columns
    ODS_Dict['TRANSACTIONS'] = ODS_Dict['TRANSACTIONS'].drop('Id', 'value', 'E_ID')

    # Perform left join with inv_df
    ODS_Dict['TRANSACTIONS'] = ODS_Dict['TRANSACTIONS'].join(inv_df, 
                                                             ODS_Dict['TRANSACTIONS']['Invoice_ID'] == inv_df['inv_ID'], 
                                                             how='left')

    # More conditional transformations
    ODS_Dict['TRANSACTIONS'] = ODS_Dict['TRANSACTIONS'].withColumn('Original_Amount', when(col('Original_Amount').isNull() | (col('Original_Amount') == "null"), col('Basci_val')).otherwise(col('Original_Amount'))) \
                                                       .withColumn('Total_Amount', when(col('Total_Amount').isNull() | (col('Total_Amount') == "null"), col('TotalPrice')).otherwise(col('Total_Amount'))) \
                                                       .withColumn('Fees', when(col('Fees').isNull() | (col('Fees') == "null"), col('added_money')).otherwise(col('Fees'))) \
                                                       .withColumn('T_Inv_ID', when((col('Invoice_ID') == col('inv_ID')), col('v_ID')).otherwise(col('T_Inv_ID'))) \
                                                       .withColumn('Is_Reversed', when(col('Is_Reversed') == "true", 1).otherwise(0))

    # Drop unnecessary columns
    ODS_Dict['TRANSACTIONS'] = ODS_Dict['TRANSACTIONS'].drop('inv_ID', 'Basci_val', 'added_money', 'TotalPrice', 'v_ID', 'Source', 'Invoice_ID')

    # Final column renaming and transformations
    ODS_Dict['TRANSACTIONS'] = ODS_Dict['TRANSACTIONS'].withColumnRenamed("Trx_id", "ID") \
                                                       .withColumnRenamed("T_Inv_ID", "Invoice_ID") \
                                                       .withColumn('Original_Amount', when(col('Original_Amount').isNull() | (col('Original_Amount') == "null"), 0).otherwise(col('Original_Amount'))) \
                                                       .withColumn('Total_Amount', when(col('Total_Amount').isNull() | (col('Total_Amount') == "null"), 0).otherwise(col('Total_Amount'))) \
                                                       .withColumn('Fees', when(col('Fees').isNull() | (col('Fees') == "null"), 0).otherwise(col('Fees'))) \
                                                       .withColumn('ID', when((col('SourceSystem') == 'UAT') | (col('SourceSystem') == 'UAT_V2'), concat(lit(new_date * -1), col('ID'))).otherwise(col('ID'))) \
                                                       .withColumn('Original_Trx', when(col('Original_Trx').isNull() | (col('Original_Trx') == "null"), 0).otherwise(col('Original_Trx'))) \
                                                       .withColumn('Request_ID', when((col('SourceSystem') == 'UAT') | (col('SourceSystem') == 'UAT_V2'), concat(lit(new_date * -1), col('Request_ID'))).otherwise(col('Request_ID'))) \
                                                       .withColumn('Invoice_ID', when((col('SourceSystem') == 'UAT') & (col('Invoice_ID') > 0), concat(lit(new_date * -1), col('Invoice_ID'))).otherwise(col('Invoice_ID'))) \
                                                       .withColumn('Is_Reversed', when(col('Is_Reversed') == "true", 1).otherwise(0))


    return ODS_Dict

#***********************************************************************************************************************************
def Requests_HaveNo_TRX(ODS_Dict ):
      Request_df = ODS_Dict['REQUESTS'].select('*').where(col('Status').isin([2,4,5]))
      Request_IDS = Request_df.join(ODS_Dict['TRANSACTIONS'] ,Request_df['ID']== ODS_Dict['TRANSACTIONS']['Request_ID'],how='left_anti' )
      Request_IDS = Request_IDS.withColumn('ID_new' ,when( (col('Status')==5)  ,concat( lit(int(curr_dt.timestamp()) * -1 )  , col('ID')  ).cast('long')  ).otherwise(col('ID')))\
                               .withColumn('ID_new',when(col('ID_new').isNull(),lit( int(curr_dt.timestamp()) * -1 ) ).otherwise(col('ID_new'))) 
      #print('****************************************************************************************************8')
      #Request_IDS.printSchema()
      #Request_IDS.select(['ID' , 'ID_new' , 'Account_ID']).where(col('ID').isNull()).show(5)
      print('----------------IDS have no trx id are-------------------------------------------')
      spark.sql('Drop TABLE Trans_df')
      spark.sql("CREATE TABLE Trans_df  (ID long, Account_ID_From Int ,Account_ID_To Int, Total_Amount Decimal ,Transaction_Type Int ,Is_Reversed Int,Original_Trx long, Date Timestamp,Original_Amount Decimal, Fees Decimal,Taxes Decimal ,Invoice_ID Int, Request_ID long ,SourceSystem varchar(100)) ")
      Request_IDS.createOrReplaceTempView("req_tbl")
      spark.sql('INSERT INTO Trans_df (ID , Account_ID_From ,Account_ID_To,Total_Amount , Transaction_Type,Is_Reversed,Original_Trx ,Date ,Original_Amount,Fees,Taxes,Invoice_ID,Request_ID ,SourceSystem) Select (-1 * R.ID) , R.Account_ID ,0, R.Amount,R.Service_Denomination_ID,0,0,R.Request_Date ,0,0,0,-1111,R.ID,R.SourceSystem FROM req_tbl R WHERE R.Status in (2,4);')
      spark.sql('INSERT INTO Trans_df (ID , Account_ID_From ,Account_ID_To,Total_Amount , Transaction_Type,Is_Reversed,Original_Trx ,Date ,Original_Amount,Fees,Taxes,Invoice_ID,Request_ID ,SourceSystem) Select (-1 * ID) , Account_ID ,0, Amount,Service_Denomination_ID,0,-(1 * ID),Request_Date ,0,0,0,-1111,ID,SourceSystem FROM req_tbl WHERE Status = 5;')
      spark.sql('INSERT INTO Trans_df (ID , Account_ID_From ,Account_ID_To,Total_Amount , Transaction_Type,Is_Reversed ,Original_Trx,Date ,Original_Amount,Fees,Taxes,Invoice_ID,Request_ID,SourceSystem) Select ID_new ,0  , Account_ID , Amount,Service_Denomination_ID,0,ID_new,Request_Date ,0,0,0,-1111,ID,SourceSystem FROM req_tbl WHERE Status= 5;')
      ODS_Dict['TRANSACTIONS'].createOrReplaceTempView('Trxs')
      ODS_Dict['TRANSACTIONS'] = spark.sql('select * from Trxs  UNION ALL select * from Trans_df')
      ODS_Dict['TRANSACTIONS'].printSchema()
      print('________________________________________________')
      #ttt = spark.sql('select * from Trans_df')
      #ttt.printSchema()
      #ODS_Dict['TRANSACTIONS'] =  ODS_Dict['TRANSACTIONS'].dropDuplicates(['ID'])
      #spark.sql('select * from Trans_df where ID IS NULL').show(10)
      print('*****************************TRX**********************************************')
      #ODS_Dict['TRANSACTIONS'].select(['ID' , 'Account_ID_From','Original_Trx']).where(col('ID').isNull()).show(10)
      #spark.sql('Drop TABLE Trans_df')
      #spark.sql("SELECT * FROM Trans_df").show()
      #---------------------------------------------------------------------------------
      #ODS_Dict['REQUESTS'] = ODS_Dict['REQUESTS'].drop('ID')
      #ODS_Dict['REQUESTS'] = ODS_Dict['REQUESTS'].drop('ID')
      #ODS_Dict['REQUESTS'] = ODS_Dict['REQUESTS'].withColumnRenamed('new_ID' , 'ID')
      #ODS_Dict['TRANSACTIONS'] = ODS_Dict['TRANSACTIONS'].drop('ID')
      #ODS_Dict['TRANSACTIONS'] = ODS_Dict['TRANSACTIONS'].withColumnRenamed('trx_ID' , 'ID')
      #ODS_Dict['TRANSACTIONS'] = ODS_Dict['TRANSACTIONS'].withColumnRenamed('trx_ID' , 'ID')
      return ODS_Dict
#***********************************************************************************************************************************
default_time = datetime(1980,1,1,0,0,0)
def Center_Transformation(ODS_Dict):
    ODS_Dict['ACCOUNTS'] = ODS_Dict['ACCOUNTS'].union(ODS_Dict['ID_ACCOUNTS'])
    ODS_Dict['ACCOUNTS'] = ODS_Dict['ACCOUNTS'].dropDuplicates(['Id'])
    del ODS_Dict['ID_ACCOUNTS']
    # ----------------------------------------------------------------------------------
    col_Names = ['NAME', 'Address', 'national_id', 'Mobile']
    # ---------------------------------------------------------------------------------------
    for name in col_Names:
        ODS_Dict['ACCOUNTS'] = ODS_Dict['ACCOUNTS'].withColumn(name, ltrim(rtrim(col(name))))\
                                                     .withColumn(name, when(length(col(name)) < 2, 'Unknown').otherwise(col(name)))
        if name == 'national_id':
            ODS_Dict['ACCOUNTS'] = ODS_Dict['ACCOUNTS'].withColumn('national_id', when((length(col('national_id')) < 14) | col('national_id').isNull() | (col('national_id') == "null"), 'Unknown').otherwise(col('national_id')))\
                                                       .withColumn('national_id', when((length(col('national_id')) > 14) & ((col('national_id').substr(1, 1) == '3') | (col('national_id').substr(1, 1) == '2')), col('national_id').substr(1, 14)).otherwise('Unknown'))
        elif name == 'Mobile':
            ODS_Dict['ACCOUNTS'] = ODS_Dict['ACCOUNTS'].withColumn('Mobile', when((col('Mobile').cast("int").isNotNull()) & (length(col('Mobile')) == 11), col('Mobile')).otherwise('Unknown'))
        ODS_Dict['ACCOUNTS'] = ODS_Dict['ACCOUNTS'].withColumn(name, when(col(name).isNull() | (col(name) == "null"), 'Unknown').otherwise(col(name)))\
                                                   .withColumn('added_date', when(col('added_date').isNull() | (col('added_date') == "null"), lit(default_time)).otherwise(col('added_date')))\
                                                   .withColumn('COMMISSIONABLE', when(length(col('COMMISSIONABLE')) > 1, 0).otherwise(col('COMMISSIONABLE')))\
                                                   .withColumn('Address', when(col('Address').isNull() | (col('Address') == "null") | (col('Address') == '? ????? ????? ???? ?????? ????? ??? ??? - ???????') | (col('Address') == '0'), 'Unknown').otherwise(col('Address')))\
                                                   .withColumn('Place_Id', when(col('Place_Id').isNull() | (col('Place_Id') == "null"), 0).otherwise(col('Place_Id')))\
                                                   .withColumn('Commissionable', when(col('Commissionable').isNull() | (col('Commissionable') == "null"), 0).otherwise(col('Commissionable')))
    
    return ODS_Dict

def AccountLocations_Transformation(ODS_Dict):
    ODS_Dict['ACCOUNT_LOCATIONS'] = ODS_Dict['ACCOUNT_LOCATIONS'].union(ODS_Dict['ID_ACCOUNT_LOCATIONS'])
    ODS_Dict['ACCOUNT_LOCATIONS'] = ODS_Dict['ACCOUNT_LOCATIONS'].dropDuplicates(['ID'])
    del ODS_Dict['ID_ACCOUNT_LOCATIONS']
    ODS_Dict['ACCOUNT_LOCATIONS'] = ODS_Dict['ACCOUNT_LOCATIONS'].withColumn('UPDATE_DATE', when(col('UPDATE_DATE').isNull() | (col('UPDATE_DATE') == "null"), lit(default_time)).otherwise(col('UPDATE_DATE')))\
                                                                 .withColumn('Account_ID', when((col('Account_ID') == 155658) | col('Account_ID').isNull() | (col('Account_ID') == "null"), 0).otherwise(col('Account_ID')))\
                                                                 .withColumn('Longitude', when(col('Longitude').isNull() | (col('Longitude') == "null"), 0).otherwise(col('Longitude')))\
                                                                 .withColumn('Latitude', when(col('Latitude').isNull() | (col('Latitude') == "null"), 0).otherwise(col('Latitude')))
    return ODS_Dict

def Channel_Transformation(ODS_Dict):
    ODS_Dict['CHANNELS'] = ODS_Dict['CHANNELS'].union(ODS_Dict['ID_CHANNELS'])
    ODS_Dict['CHANNELS'] = ODS_Dict['CHANNELS'].dropDuplicates(['ID'])
    del ODS_Dict['ID_CHANNELS']
    ODS_Dict['CHANNELS'] = ODS_Dict['CHANNELS'].withColumn('Serial', when(col('Serial').isNull() | (col('Serial') == "null"), '0').otherwise(col('Serial')))
    return ODS_Dict

def Channel_Identifier(ODS_Dict):
    ODS_Dict['CHANNEL_IDENTIFIERS'] = ODS_Dict['CHANNEL_IDENTIFIERS'].union(ODS_Dict['ID_CHANNEL_IDENTIFIERS'])
    ODS_Dict['CHANNEL_IDENTIFIERS'] = ODS_Dict['CHANNEL_IDENTIFIERS'].dropDuplicates(['ID'])
    del ODS_Dict['ID_CHANNEL_IDENTIFIERS']
    ODS_Dict['CHANNEL_IDENTIFIERS'] = ODS_Dict['CHANNEL_IDENTIFIERS'].withColumn('UpdateDate', when(col('UpdateDate').isNull() | (col('UpdateDate') == "null"), default_time).otherwise(col('UpdateDate')))\
                                                                    .withColumn('UpdatedBy', when(col('UpdatedBy').isNull() | (col('UpdatedBy') == "null"), 0).otherwise(col('UpdatedBy')))\
                                                                    .withColumn('Value', when(col('Value').isNull() | (col('Value') == "null"), 'Unknown').otherwise(col('Value')))
    return ODS_Dict

def Account_TrxCommission(ODS_Dict):
    Account_Trx_df = ODS_Dict['ACCOUNT_TRANSACTION_COMMISSION'].union(ODS_Dict['TMS_ACCOUNT_TRANSACTION_COMMISSION'])
    New_Account_df = Account_Trx_df.union(ODS_Dict['UAT_ACCOUNT_TRANSACTION_COMMISSION'])
    ODS_Dict['ACCOUNT_TRANSACTION_COMMISSION'] = New_Account_df.union(ODS_Dict['TMS_UAT_ACCOUNT_TRANSACTION_COMMISSION'])
    del ODS_Dict['TMS_ACCOUNT_TRANSACTION_COMMISSION']
    del ODS_Dict['UAT_ACCOUNT_TRANSACTION_COMMISSION']
    del ODS_Dict['TMS_UAT_ACCOUNT_TRANSACTION_COMMISSION']
    ODS_Dict['ACCOUNT_TRANSACTION_COMMISSION'] = ODS_Dict['ACCOUNT_TRANSACTION_COMMISSION'].withColumn('ID', when((col('Source') == 'UAT') | (col('Source') == 'UAT_V2'), concat(lit(new_date * -1), col('ID'))).otherwise(col('ID')))\
                                                                                           .withColumn('Transaction_ID', when((col('Source') == 'UAT') | (col('Source') == 'UAT_V2'), concat(lit(new_date * -1), col('Transaction_ID'))).otherwise(col('Transaction_ID')))
    ODS_Dict['ACCOUNT_TRANSACTION_COMMISSION'] = ODS_Dict['ACCOUNT_TRANSACTION_COMMISSION'].drop('Source')
    ODS_Dict['ACCOUNT_TRANSACTION_COMMISSION'] = ODS_Dict['ACCOUNT_TRANSACTION_COMMISSION'].dropDuplicates(['ID'])
    return ODS_Dict

def Demomination_Transformation(ODS_Dict):
    ODS_Dict['DENOMINATION_SERVICE_PROVIDER'] = ODS_Dict['DENOMINATION_SERVICE_PROVIDER'].union(ODS_Dict['TMS_DENOMINATION_SERVICE_PROVIDER'])
    ODS_Dict['DENOMINATION_SERVICE_PROVIDER'] = ODS_Dict['DENOMINATION_SERVICE_PROVIDER'].withColumn('OLD_SERVICE_ID', when(col('OLD_SERVICE_ID').isNull() | (col('OLD_SERVICE_ID') == "null"), 0).otherwise(col('OLD_SERVICE_ID')))\
                                                                                         .withColumn('ProviderAmount', when(col('ProviderAmount').isNull() | (col('ProviderAmount') == "null"), 0).otherwise(col('ProviderAmount')))\
                                                                                         .withColumn('ID', when(col('Source') == 'GW_V2', -col('ID')).otherwise(col('ID')))\
                                                                                         .withColumn('Service_Provider_ID', when(col('Source') == 'GW_V2', -col('Service_Provider_ID')).otherwise(col('Service_Provider_ID')))
    del ODS_Dict['TMS_DENOMINATION_SERVICE_PROVIDER']
    ODS_Dict["DENOMINATIONS"] = ODS_Dict["DENOMINATIONS"].withColumn('PaymentTypeID', when(col('PaymentTypeID').isNull() | (col('PaymentTypeID') == "null"), 0).otherwise(col('PaymentTypeID')))
    return ODS_Dict

def Account_type_Transformation(ODS_Dict):
    ODS_Dict['ACCOUNT_TYPE'] = ODS_Dict['ACCOUNT_TYPE'].union(ODS_Dict['ID_ACCOUNT_TYPE'])
    ODS_Dict['ACCOUNT_TYPE'] = ODS_Dict['ACCOUNT_TYPE'].dropDuplicates(['ID'])
    del ODS_Dict['ID_ACCOUNT_TYPE']
    ODS_Dict['ACCOUNT_TYPE'] = ODS_Dict['ACCOUNT_TYPE'].withColumn('NAME', when(col('NAME').isNull() | (col('NAME') == "null"), 'Unknown').otherwise(col('NAME')))
    return ODS_Dict
   #---------------------------------------------------------------------------------------
   
   #insert herrrrrrr
def Channel_type_Transfomation(ODS_Dict):
    ODS_Dict['CHANNEL_TYPES'] =  ODS_Dict['CHANNEL_TYPES'].union(ODS_Dict['ID_CHANNEL_TYPES'])
    ODS_Dict['CHANNEL_TYPES'] = ODS_Dict['CHANNEL_TYPES'].dropDuplicates(['ID'])
    del ODS_Dict['ID_CHANNEL_TYPES']
    return ODS_Dict
  #--------------------------------------------------------------------------------------

def ID_tables_Transformation(ODS_Dict):
    ODS_Dict['Account_Type_Profile'] = ODS_Dict['Account_Type_Profile'].withColumn(
        'UpdateDate', when(col('UpdateDate').isNull() | (col('UpdateDate') == 'null'), lit(default_time)).otherwise(col('UpdateDate'))
    )
    ODS_Dict['GOVERNORATES'] = ODS_Dict['GOVERNORATES'].withColumn(
        'UpdateDate', when(col('UpdateDate').isNull() | (col('UpdateDate') == 'null'), lit(default_time)).otherwise(col('UpdateDate'))
    )
    ODS_Dict['Inquiry_Bill'] = ODS_Dict['Inquiry_Bill'].withColumn(
        'Mandatory', when(col('Mandatory') == True, 1).otherwise(0)
    )
    return ODS_Dict


def Services_Transformation(ODS_Dict):
    ODS_Dict['SERVICE'] = ODS_Dict['SERVICE'].withColumn(
        'SERVICE_GROUP_ID', when(col('SERVICE_GROUP_ID').isNull() | (col('SERVICE_GROUP_ID') == 'null'), 0).otherwise(col('SERVICE_GROUP_ID'))
    )
    return ODS_Dict


def Account_channel_transformation(ODS_Dict):
    ODS_Dict['ACCOUNT_CHANNELS'] = ODS_Dict['ACCOUNT_CHANNELS'].withColumn(
        'UpdateDate', when(col('UpdateDate').isNull() | (col('UpdateDate') == 'null'), lit(default_time)).otherwise(col('UpdateDate'))
    ).withColumn(
        'UpdatedBy', when(col('UpdatedBy').isNull() | (col('UpdatedBy') == 'null'), 0).otherwise(col('UpdatedBy'))
    ).withColumn(
        'ChannelID', when((col('ChannelID') == 242078) | (col('ChannelID') == 225678), 21).otherwise(col('ChannelID'))
    )
    return ODS_Dict

#*******************************************************************************

def Requests_Provider_Details(ODS_Dict):
    # Union with UAT data and clean up
    ODS_Dict['Request_Provider_Details'] = ODS_Dict['Request_Provider_Details'].union(ODS_Dict['UAT_Request_Provider_Details'])
    del ODS_Dict['UAT_Request_Provider_Details']

    # Ensure new_date is defined appropriately
    new_date = 20240619  # Example value; replace with your actual logic to compute new_date

    # Transformations on Request_Provider_Details
    ODS_Dict['Request_Provider_Details'] = ODS_Dict['Request_Provider_Details'].withColumn(
        'ProviderFees', when(col('ProviderFees').isNull() | (col('ProviderFees') == 'null'), "0").otherwise(col('ProviderFees'))
    ).withColumn(
        'ProviderTransactionId', when(col('ProviderTransactionId').isNull() | (col('ProviderTransactionId') == 'null'), "0").otherwise(col('ProviderTransactionId'))
    ).withColumn(
        'RequestID', when(col('Source') == 'UAT', concat(lit(new_date * -1), col('RequestID'))).otherwise(col('RequestID'))
    ).withColumn(
        'ID', when(col('Source') == 'UAT', concat(lit(new_date * -1), col('ID'))).otherwise(col('ID'))
    )

    ODS_Dict['Request_Provider_Details'] = ODS_Dict['Request_Provider_Details'].drop('Source')

    # Separate processing for specific service denomination and success/failure criteria
    df = ODS_Dict['Request_Provider_Details'].select('*').where(col('Service_Denomination_ID') == 507)
    Success_df = df.select('*').where((col('ProviderTransactionID').substr(1, 1) == '0'))
    Success_df = Success_df.dropDuplicates(['RequestID'])
    failed_df = df.join(Success_df, df['RequestID'] == Success_df['RequestID'], how='left_anti')
    failed_df = failed_df.dropDuplicates(['RequestID'])
    concated_df = Success_df.union(failed_df)

    # Union of processed data back into Request_Provider_Details
    ODS_Dict['Request_Provider_Details'] = ODS_Dict['Request_Provider_Details'].filter(col('Service_Denomination_ID') != 507)
    ODS_Dict['Request_Provider_Details'] = ODS_Dict['Request_Provider_Details'].union(concated_df)

    return ODS_Dict

   
def ProviderTransformations(ODS_Dict):
    ODS_Dict['ProviderServiceRequests'] = ODS_Dict['ProviderServiceRequests'].union(ODS_Dict['UAT_ProviderServiceRequests'])
    del ODS_Dict['UAT_ProviderServiceRequests']
    ODS_Dict['ProviderServiceRequests'] = ODS_Dict['ProviderServiceRequests'].withColumn(
        'Brn', when(col('Brn').isNull() | (col('Brn') == 'null'), 0).otherwise(col('Brn'))
    ).withColumn(
        'BillingAccount', when(col('BillingAccount').isNull() | (col('BillingAccount') == 'null'), 'NA').otherwise(col('BillingAccount'))
    ).withColumn(
        'UpdateDate', when(col('UpdateDate').isNull() | (col('UpdateDate') == 'null'), lit(default_time)).otherwise(col('UpdateDate'))
    ).withColumn(
        'ID', when((col('Source') == 'UAT_V2'), concat(lit(new_date * -1), col('ID'))).otherwise(col('ID'))
    ).withColumn(
        'Brn', when((col('Source') == 'UAT_V2'), concat(lit(new_date * -1), col('Brn'))).otherwise(col('Brn'))
    )
    ODS_Dict['ProviderServiceRequests'] = ODS_Dict['ProviderServiceRequests'].dropDuplicates(['ID'])
    ODS_Dict['ProviderServiceRequests'] = ODS_Dict['ProviderServiceRequests'].drop('Source')
    #---------------------------------------------------------------------------
    ODS_Dict['ProviderServiceResponse'] = ODS_Dict['ProviderServiceResponse'].union(ODS_Dict['UAT_ProviderServiceResponse'])
    ODS_Dict['ProviderServiceResponse'] = ODS_Dict['ProviderServiceResponse'].withColumn(
        'ID', when((col('Source') == 'UAT_V2'), concat(lit(new_date * -1), col('ID'))).otherwise(col('ID'))
    ).withColumn(
        'Provider_Service_Request_ID', when((col('Source') == 'UAT_V2'), concat(lit(new_date * -1), col('Provider_Service_Request_ID'))).otherwise(col('Provider_Service_Request_ID'))
    )
    ODS_Dict['ProviderServiceResponse'] = ODS_Dict['ProviderServiceResponse'].dropDuplicates(['ID'])
    ODS_Dict['ProviderServiceResponse'] = ODS_Dict['ProviderServiceResponse'].drop('Source')
    del ODS_Dict['UAT_ProviderServiceResponse']
    #--------------------------------------------------------------------------
    ODS_Dict['ProviderServiceResponseParams'] = ODS_Dict['ProviderServiceResponseParams'].union(ODS_Dict['UAT_ProviderServiceResponseParams'])
    #ODS_Dict['ProviderServiceResponseParams'] = ODS_Dict['ProviderServiceResponseParams'].dropDuplicates(['ID'])
    del ODS_Dict['UAT_ProviderServiceResponseParams']
    ODS_Dict['ProviderServiceResponseParams'] = ODS_Dict['ProviderServiceResponseParams'].withColumn(
        'Brn', when(col('Brn').isNull() | (col('Brn') == 'null'), 0).otherwise(col('Brn'))
    ).withColumn(
        'Brn', when((col('Source') == 'UAT_V2'), concat(lit(new_date * -1), col('Brn'))).otherwise(col('Brn'))
    ).withColumn(
        'PROVIDER_SERVICE_RESPONSE_ID', when((col('Source') == 'UAT_V2'), concat(lit(new_date * -1), col('PROVIDER_SERVICE_RESPONSE_ID'))).otherwise(col('PROVIDER_SERVICE_RESPONSE_ID'))
    ).withColumn(
        'Value', when(col('Value').isNull() | (col('Value') == 'null'), 'NA').otherwise(col('Value'))
    ).withColumn(
        'ID', when((col('Source') == 'UAT_V2'), concat(lit(new_date * -1), col('ID'))).otherwise(col('ID'))
    )
    ODS_Dict['ProviderServiceResponseParams'] = ODS_Dict['ProviderServiceResponseParams'].drop('Source')
    #--------------------------------------------------------------------------
    
    return ODS_Dict

#******************************************************************************

#herrrrrrrrrrrrrr
def Balance_Transformation(ODS_Dict, inv_df, electronic_charge, electronicVoucher_df):
    ODS_Dict['BALANCE'] = ODS_Dict['BALANCE'].union(ODS_Dict['UAT_BALANCE'])
    del ODS_Dict['UAT_BALANCE']
    electronic_charge = electronic_charge.withColumnRenamed('Id', 'electronic_Id')
    electronicVoucher_df = electronicVoucher_df.withColumnRenamed('Id', 'electronicV_Id')
    inv_df = inv_df.drop('Source')
    
    ODS_Dict['BALANCE'] = ODS_Dict['BALANCE'].join(inv_df, ODS_Dict['BALANCE']['InvId'] == inv_df['inv_ID'], how='left')
    ODS_Dict['BALANCE'] = ODS_Dict['BALANCE'].withColumn(
        'newInv_id', when((col('InvId') == col('inv_ID')), col('v_ID')).otherwise(col('InvId'))
    )
    ODS_Dict['BALANCE'] = ODS_Dict['BALANCE'].drop('inv_ID', 'Basci_val', 'added_money', 'TotalPrice', 'v_ID')
    
    ODS_Dict['BALANCE'] = ODS_Dict['BALANCE'].join(electronic_charge, ODS_Dict['BALANCE']['InvId'] == electronic_charge['electronic_Id'], how='left')
    ODS_Dict['BALANCE'] = ODS_Dict['BALANCE'].withColumn(
        'newInv_id', when((col('InvId') == col('electronic_Id')), col('E_ID')).otherwise(col('newInv_id'))
    )
    ODS_Dict['BALANCE'] = ODS_Dict['BALANCE'].drop('electronic_Id', 'value', 'E_ID')
    
    ODS_Dict['BALANCE'] = ODS_Dict['BALANCE'].join(electronicVoucher_df, ODS_Dict['BALANCE']['InvId'] == electronicVoucher_df['electronicV_Id'], how='left')
    ODS_Dict['BALANCE'] = ODS_Dict['BALANCE'].withColumn(
        'newInv_id', when((col('InvId') == col('electronicV_Id')), col('ev_ID')).otherwise(col('newInv_id'))
    )
    ODS_Dict['BALANCE'] = ODS_Dict['BALANCE'].drop('electronicV_Id', 'value', 'ev_ID')
    
    ODS_Dict['BALANCE'] = ODS_Dict['BALANCE'].withColumn('InvId', col('newInv_id'))
    ODS_Dict['BALANCE'] = ODS_Dict['BALANCE'].drop('newInv_id')
    
    ODS_Dict['BALANCE'] = ODS_Dict['BALANCE'].filter(col('CenterId') != 0)
    ODS_Dict['BALANCE'] = ODS_Dict['BALANCE'].withColumn(
        'InvId', when(col('InvId').isNull() | (col('InvId') == 'null'), -1).otherwise(col('InvId'))
    ).withColumn(
        'Descrp', when(col('Descrp').isNull() | (col('Descrp') == 'null'), 'Unknown').otherwise(col('Descrp'))
    ).withColumn(
        'AddedTime', when(col('AddedTime').isNull() | (col('AddedTime') == 'null'), lit(default_time)).otherwise(col('AddedTime'))
    ).withColumn(
        'ServId', when(col('ServId').isNull() | (col('ServId') == 'null'), 0).otherwise(col('ServId'))
    ).withColumn(
        'CenterId', when(col('CenterId').isNull() | (col('CenterId') == 'null'), -111).otherwise(col('CenterId'))
    ).withColumn(
        'Amount', when(col('Amount').isNull() | (col('Amount') == 'null'), 0).otherwise(col('Amount'))
    ).withColumn(
        'Before', when(col('Before').isNull() | (col('Before') == 'null'), 0).otherwise(col('Before'))
    ).withColumn(
        'tel', when(length(col('tel')) <= 40, col('tel')).otherwise('Unknown')
    ).withColumn(
        'tel', when(col('tel').isNull() | (col('tel') == 'null'), 'Unknown').otherwise(col('tel'))
    ).withColumn(
        'Id', when(col('Source') == 'UAT', concat(lit(new_date * -1), col('Id'))).otherwise(col('Id'))
    ).withColumn(
        'InvId', when((col('Source') == 'UAT') & (col('InvId') > 0), concat(lit(new_date * -1), col('InvId'))).otherwise(col('InvId'))
    )
    
    ODS_Dict['BALANCE'] = ODS_Dict['BALANCE'].dropDuplicates(['Id'])
    return ODS_Dict

def Request_Status(ODS_Dict):
    ODS_Dict['REQUEST_STATUS'] = ODS_Dict['REQUEST_STATUS'].withColumn(
        'Response_Code', when(col('Response_Code').isNull() | (col('Response_Code') == 'null'), 'Unknown').otherwise(col('Response_Code'))
    ).withColumn(
        'Description', when(col('Description').isNull() | (col('Description') == 'null'), 'Unknown').otherwise(col('Description'))
    )

def failed_request_statusCode(ODS_Dict):
    ODS_Dict['FailedRequestStatusCodes'] = ODS_Dict['FailedRequestStatusCodes'].union(ODS_Dict['UAT_FailedRequestStatusCodes'])
    del ODS_Dict['UAT_FailedRequestStatusCodes']
    return ODS_Dict

def Inquiry_Bills(ODS_Dict):
    ODS_Dict['Inquiry_BILLS_Details'] = ODS_Dict['Inquiry_BILLS_Details'].union(ODS_Dict['UAT_Inquiry_BILLS_Details'])
    ODS_Dict['Inquiry_BILLS_Details'] = ODS_Dict['Inquiry_BILLS_Details'].withColumn(
        'ID', when(col('Source') == 'UAT_V2', concat(lit(new_date * -1), col('ID'))).otherwise(col('ID'))
    ).withColumn(
        'InquiryBillID', when(col('Source') == 'UAT_V2', concat(lit(new_date * -1), col('InquiryBillID'))).otherwise(col('InquiryBillID'))
    )
    ODS_Dict['Inquiry_BILLS_Details'] = ODS_Dict['Inquiry_BILLS_Details'].drop('Source')
    
    ODS_Dict['Inquiry_Bill'] = ODS_Dict['Inquiry_Bill'].union(ODS_Dict['UAT_Inquiry_Bill'])
    ODS_Dict['Inquiry_Bill'] = ODS_Dict['Inquiry_Bill'].withColumn(
        'ID', when(col('Source') == 'UAT_V2', concat(lit(new_date * -1), col('ID'))).otherwise(col('ID'))
    )
    ODS_Dict['Inquiry_Bill'] = ODS_Dict['Inquiry_Bill'].drop('Source')
    
    ODS_Dict['Inquiry_BILLS_Details'] = ODS_Dict['Inquiry_BILLS_Details'].dropDuplicates(['ID'])
    ODS_Dict['Inquiry_Bill'] = ODS_Dict['Inquiry_Bill'].dropDuplicates(['ID'])
    del ODS_Dict['UAT_Inquiry_BILLS_Details']
    del ODS_Dict['UAT_Inquiry_Bill']
    return ODS_Dict

def Logs(ODS_Dict):
    gw_logs = ODS_Dict['LOGS'].union(ODS_Dict['UAT_LOGS'])
    gw_logs.printSchema()
    TMS_logs = ODS_Dict['TMS_LOGS'].union(ODS_Dict['UAT_TMS_LOGS'])
    TMS_logs.printSchema()
    ODS_Dict['LOGS'] = gw_logs.union(TMS_logs)
    del ODS_Dict['UAT_LOGS']
    del ODS_Dict['UAT_TMS_LOGS']
    del ODS_Dict['TMS_LOGS']
    return ODS_Dict

def Reciept_param(ODS_Dict):
    ODS_Dict['ReceiptBodyParams'] = ODS_Dict['ReceiptBodyParams'].union(ODS_Dict['UAT_ReceiptBodyParams'])
    ODS_Dict['ReceiptBodyParams'] = ODS_Dict['ReceiptBodyParams'].withColumn(
        'ID', when(col('Source') == 'UAT_V2', concat(lit(new_date * -1), col('ID'))).otherwise(col('ID'))
    ).withColumn(
        'Transaction_ID', when(col('Source') == 'UAT_V2', concat(lit(new_date * -1), col('Transaction_ID'))).otherwise(col('Transaction_ID'))
    ).withColumn(
        'Provider_Service_Request_ID', when(col('Source') == 'UAT_V2', concat(lit(new_date * -1), col('Provider_Service_Request_ID'))).otherwise(col('Provider_Service_Request_ID'))
    )
    ODS_Dict['ReceiptBodyParams'] = ODS_Dict['ReceiptBodyParams'].dropDuplicates(['ID'])
    ODS_Dict['ReceiptBodyParams'] = ODS_Dict['ReceiptBodyParams'].drop('Source')
    del ODS_Dict['UAT_ReceiptBodyParams']
    return ODS_Dict

def Transfer_req(ODS_Dict):
    ODS_Dict['TransferRequests'] = ODS_Dict['TransferRequests'].union(ODS_Dict['UAT_TransferRequests'])
    ODS_Dict['TransferRequests'] = ODS_Dict['TransferRequests'].withColumn(
        'ID', when(col('Source') == 'UAT_V2', concat(lit(new_date * -1), col('ID'))).otherwise(col('ID'))
    )
    ODS_Dict['TransferRequests'] = ODS_Dict['TransferRequests'].drop('Source')
    ODS_Dict['TransferRequests'] = ODS_Dict['TransferRequests'].dropDuplicates(['ID'])
    del ODS_Dict['UAT_TransferRequests']
    return ODS_Dict

def Balances_History_Hold(ODS_Dict):
    ODS_Dict['BalanceHistories'] = ODS_Dict['BalanceHistories'].union(ODS_Dict['UAT_BalanceHistories'])
    del ODS_Dict['UAT_BalanceHistories']
    ODS_Dict['BalanceHistories'] = ODS_Dict['BalanceHistories'].withColumn(
        'ID', when(col('Source') == 'UAT_V2', concat(lit(new_date * -1), col('ID'))).otherwise(col('ID'))
    ).withColumn(
        'RequestID', when(col('Source') == 'UAT_V2', concat(lit(new_date * -1), col('RequestID'))).otherwise(col('RequestID'))
    )
    
    ODS_Dict['HoldBalances'] = ODS_Dict['HoldBalances'].union(ODS_Dict['UAT_HoldBalances'])
    del ODS_Dict['UAT_HoldBalances']
    ODS_Dict['HoldBalances'] = ODS_Dict['HoldBalances'].withColumn(
        'ID', when(col('Source') == 'UAT_V2', concat(lit(new_date * -1), col('ID'))).otherwise(col('ID'))
    ).withColumn(
        'RequestID', when(col('Source') == 'UAT_V2', concat(lit(new_date * -1), col('RequestID'))).otherwise(col('RequestID'))
    )
    return ODS_Dict

def electricty_log_tranformation(ODS_Dict):
    ODS_Dict['Electricity_log'] = ODS_Dict['Electricity_log'].withColumn(
        'amount', when(col('amount').isNull() | (col('amount') == 'null'), 0).otherwise(col('amount'))
    ).withColumn(
        'ProviderTransactionId', when(col('ProviderTransactionId').isNull() | (col('ProviderTransactionId') == 'null'), 'Unknown').otherwise(col('ProviderTransactionId'))
    ).withColumn(
        'InvId', when(col('InvId').isNull() | (col('InvId') == 'null'), 0).otherwise(col('InvId'))
    )

def PengingPaymentCard_tranformation(ODS_Dict):
    ODS_Dict['PengingPaymentCard'] = ODS_Dict['PengingPaymentCard'].withColumn(
        'UpdatedDate', when(col('UpdatedDate').isNull() | (col('UpdatedDate') == 'null'), lit(default_time)).otherwise(col('UpdatedDate'))
    )

def Service_Provider(ODS_Dict):
    ODS_Dict['SERVICE_PROVIDER'] = ODS_Dict['SERVICE_PROVIDER'].union(ODS_Dict['TMS_SERVICE_PROVIDER'])
    ODS_Dict['SERVICE_PROVIDER'] = ODS_Dict['SERVICE_PROVIDER'].withColumn(
        'ID', when(col('Source') == 'GW_V2', -col('ID')).otherwise(col('ID'))
    )
    del ODS_Dict['TMS_SERVICE_PROVIDER']
    return ODS_Dict

def PROVIDER_SERVICE_REQUESTS_Transformation(ODS_Dict):
    GW_Requests = ODS_Dict['PROVIDER_SERVICE_REQUESTS'].union(ODS_Dict['UAT_PROVIDER_SERVICE_REQUESTS'])
    TMS_Requests = ODS_Dict['TMS_PROVIDER_SERVICE_REQUESTS'].union(ODS_Dict['UAT_TMS_PROVIDER_SERVICE_REQUESTS'])
    ODS_Dict['PROVIDER_SERVICE_REQUESTS'] = GW_Requests.union(TMS_Requests)
    del ODS_Dict['TMS_PROVIDER_SERVICE_REQUESTS']
    del ODS_Dict['UAT_PROVIDER_SERVICE_REQUESTS']
    del ODS_Dict['UAT_TMS_PROVIDER_SERVICE_REQUESTS']
    return ODS_Dict
#*********************************************************************************************************************************************************
def Main():
    load_dotenv()   
    STG_user = os.getenv("STG_User")
    STG_pass = os.getenv("STG_Pass")
    #-------------- Extract --------------------------------
    
    
    date_From = (datetime.now() - timedelta(1)).strftime('%Y-%m-%d') + ' 00:00:00'
    date_To = (datetime.now() - timedelta(0)).strftime('%Y-%m-%d') + ' 00:00:00'
    
    #date_From = '2024-06-23' + ' 00:00:00'
    #date_To = '2024-06-24' + ' 00:00:00'
    
    Queries = Adding_Date(ODS_Queries, date_From, date_To)
    ODS_Dict = reading_STG_DORIS('root' , '' ,'STAGING', Queries)
    #---------- Transformation ----------------------------
    ODS_Dict = Requests_Transformation(ODS_Dict)
    ODS_Dict = PROVIDER_SERVICE_REQUESTS_Transformation(ODS_Dict)
    ODS_Dict = Requests_Provider_Details(ODS_Dict)
    Request_Status(ODS_Dict)
    ODS_Dict ,inv_df , electronic_charge ,electronicVoucher_df = Invoices_Transformation(ODS_Dict)
    ODS_Dict =  Transaction_Transformation(ODS_Dict,inv_df , electronic_charge ,electronicVoucher_df)
    ODS_Dict = Balance_Transformation(ODS_Dict,inv_df , electronic_charge ,electronicVoucher_df)
    #ODS_Dict = Requests_HaveNo_TRX(ODS_Dict)
    ODS_Dict = Balances_History_Hold(ODS_Dict)
    ODS_Dict = Account_TrxCommission(ODS_Dict)
    print('----------------------------------------writting Then will start ----------------------------------------------------------------------------------')
    ODS_Dict = Center_Transformation(ODS_Dict)
    ODS_Dict = AccountLocations_Transformation(ODS_Dict)
    ODS_Dict = Channel_Transformation(ODS_Dict)
    ODS_Dict = Channel_Identifier(ODS_Dict)
    Demomination_Transformation(ODS_Dict)
    ODS_Dict = Account_type_Transformation(ODS_Dict)
    ODS_Dict = Channel_type_Transfomation(ODS_Dict)
    ID_tables_Transformation(ODS_Dict)
    Services_Transformation(ODS_Dict)
    Account_channel_transformation(ODS_Dict)
    ODS_Dict = failed_request_statusCode(ODS_Dict)
    ODS_Dict = Inquiry_Bills(ODS_Dict)
    #ODS_Dict = Logs(ODS_Dict)
    ODS_Dict = Reciept_param(ODS_Dict)
    ODS_Dict = Transfer_req(ODS_Dict)
    ProviderTransformations(ODS_Dict)
    electricty_log_tranformation(ODS_Dict)
    PengingPaymentCard_tranformation(ODS_Dict)
    ODS_Dict =Service_Provider(ODS_Dict) 
    
    #---------------LOAD -----------------------------------
    #ODS_Dict['ACCOUNTS'].show()
  
    Dict = writting_DIM('root' , '' , 'ODS' , ODS_Dict)
    
    
    write_to_oracle('root', '', 'ODS', Dict)
    
   
    

try:
    Main()
except Exception as e:
    exc=str(e)[:3000]
    #Teleg.telegram_bot_sendtext(f"An exception occurred in >>>ODS<<<: {exc}") 
    raise
