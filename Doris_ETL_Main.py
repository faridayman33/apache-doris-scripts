import pyspark.sql.functions
import requests
import pyspark
from pyspark.sql import SparkSession 
from pyspark.sql.functions import *
import os
from dotenv import load_dotenv
from datetime import datetime, timedelta
import pydoris
from pyspark.sql.types import *
from decimal import Decimal
try:
    import jaydebeapi
except ImportError:
    import sys
    import subprocess
    subprocess.check_call([sys.executable, "-m", "pip", "install", "jaydebeapi"])
    import jaydebeapi
    

#from tashaphyne.tokenizers import word_tokenize
#-----------------------------------------
spark = SparkSession.builder.appName('STG_DORIS').enableHiveSupport().config("spark.jars", "/home/Application/Apache_Spark/ETL/Doris/mysql-connector-j-8.2.0.jar").getOrCreate()

executor_cores = spark.sparkContext.getConf().get("spark.sql.encoding")

print("Number of executor cores", executor_cores)
#-----------------------------------------
GW_PROD_Tables={
     
      "CENTERS" : """SELECT  [Id]
      ,[CentralName]
      ,[CenterName]
      ,[OwnerName]
      ,[Address]
      ,[Mobile]
      ,[Phone]
      ,[Balance]
      ,[Email]
      ,[CommNo]
      ,[TaxNo]
      ,[Active]
      ,[Cat]
      ,[Last_Cat]
      ,[ChgTime]
      ,[AllowTransfeer]
      ,[user_created]
      ,[added_date]
      ,[region]
      ,[sales_person]
      ,[national_id]
      ,[activity]
      ,[sim_no]
      ,[Deal_way]
      ,[is_agent]
      ,[govern]
      ,[Mobile2]
      ,[inserted]
      ,[Sales_id]
      ,[Machine_Type]
      ,[Total]
      ,[Paid]
      ,[Remain]
      ,[Date_To]
      ,[UserUpdate]
      ,[MachineCode]
      ,[edDesc]
      ,[Logged_In]
      ,[Last_login]
      ,[last_logout]
      ,[last_channel]
      ,[Web]
      ,[Api]
      ,[Total_Amount]
      ,[Total_Points]
      ,[Serial]
      ,[Simno]
      ,[region_mina]
      ,[Place_Id]
      ,[sales_center_id]
      ,[Balance_cash]
      ,[last1month]
      ,[last2month]
      ,[last3month]
      ,[last0month]
      ,[Centers_company_Id]
      ,[Parent_CenterID]
      ,[Total_Parent_Amount]
      ,[ProfitDailyControl]
      ,[AccountTypeID]
      ,[CreatedBy]
      ,[TransferTree]
      ,[AccountProfileID]
      ,[CountResetPassword]
      ,[AllowSelfTransfer]
      ,[ApkVersion] FROM Centers""",
      
     "SERVICEPROVIDER":  """SELECT ID,Name FROM ServiceProvider

		UNION ALL

		SELECT ID,Name
		FROM [10.90.3.146].momken.dbo.ServiceProvider S1
		WHERE NOT EXISTS 
		(SELECT ID FROM ServiceProvider S2 WHERE S1.ID = S2.ID)""",

     "PARENTGROUPS": """SELECT ID,NAME,AR_NAME FROM ParentGroups""",

     'SERVICEGROUPS' : """SELECT ID,Name,ArName,ParentGroupID FROM ServiceGroups""",

     'SERVICE' : """SELECT ID,Name,ArName,ServiceTypeID,ServiceCategoryID,Code,ServiceEntityID,PathClass,ServiceGroupID FROM Service

		    UNION ALL

		    SELECT ID,Name,ArName,ServiceTypeID,ServiceCategoryID,Code,ServiceEntityID,PathClass,ServiceGroupID  
		    FROM [10.90.3.146].momken.dbo.Service S1
		    WHERE NOT EXISTS 
		    (SELECT ID FROM Service S2 WHERE S1.ID = S2.ID)""",

     'SERVICES' : """SELECT Id, ProviderId, Name, Active FROM Services""",

     'TRANSACTIONS_TYPES' : """SELECT type_id,type_name FROM transactions_types""",

     'PROVIDER' : """SELECT Id,Name,Rank FROM Provider""",

     'DENOMINATIONS' : """SELECT ID,Name,Value,ServiceID,OldDenominationID,PaymentTypeID,Status,CurrencyID,APIValue,[MinValue] as "MinValue" ,ServiceCategoryID,PathClass,Inquirable,Footer,BillPaymentModeID,NameAr FROM Denominations

			  UNION ALL

			  SELECT ID,Name,Value,ServiceID,OldDenominationID,PaymentTypeID,Status,CurrencyID,APIValue,[MinValue] as "MinValue" ,ServiceCategoryID,PathClass,Inquirable,Footer,BillPaymentModeID,NameAr
			  FROM [10.90.3.146].momken.dbo.Denominations D1
			  WHERE NOT EXISTS 
			  (SELECT ID FROM Denominations D2 WHERE D1.ID = D2.ID)""",


     'DENOMINATIONSERVICEPROVIDER' :
		 """SELECT ID,DenominationID,ServiceProviderID,Balance,Status,ProviderCode,ProviderAmount,OldServiceId,ProviderHasFees FROM DenominationServiceProvider
		

		UNION ALL

		SELECT ID,DenominationID,ServiceProviderID,Balance,Status,ProviderCode,ProviderAmount,OldServiceId,ProviderHasFees
		FROM [10.90.3.146].momken.dbo.DenominationServiceProvider S1
		WHERE NOT EXISTS 
		(SELECT DenominationID FROM DenominationServiceProvider S2 WHERE S1.ID = S2.ID)""",

     'PLACE': """SELECT Id,PlaceName,GovernorateId,PlaceType FROM Place""",

     'ACCOUNTTYPE' : """SELECT ID,Name,NameAr,Status FROM AccountType""",

     'SERVICESFIELDS' : """SELECT SFId,ServId,FieldName,FieldType,Req,fRank,Printed,Display,printed_rank,EnglishFieldName FROM ServicesFields""",

     'INVOICEDETAILS' : """SELECT  IDS.Id , IDS.InvId, IDS.SFId, IDS.Value  FROM InvoiceDetails IDS JOIN Invoice I ON IDS.InvId = I.Id WHERE CAST(I.AddedTime AS DATE )  BETWEEN '{date_from}' AND '{date_to}' """,

     'REQUESTSTATUS' : """SELECT ID,Description,ResponseCode,DescriptionAr FROM RequestStatus""",

     'REQUESTS' : """SELECT ID,UUID,AccountID,ServiceDenominationID,Status,TransactionID,UserID,RRN,RequestDate,ResponseDate,Amount,BillingAccount,ChannelID,ProviderServiceRequestID,1 as Payment_Method,0 as Ref_Number FROM Requests WHERE CAST(RequestDate AS DATE )  BETWEEN '{date_from}' AND '{date_to}'""",

     'ELECTRONIC_CHARGE'  : """SELECT Id,provider_company,mobile_number,value,invoice_price,charge_status,status_transfer,UserId,CenterId,AddedTime,UserPayedId,cause,Center_Name,provider,ServId,InCome,InComeStatus,IncomeBalanceID,Provider_Response,charge,Log_Id,InCome_cash FROM electronic_charge WHERE CAST(AddedTime AS DATE )  BETWEEN '{date_from}' AND '{date_to}' """,

     'ELECTRONIC_CHARGE_VOUCHER' : """SELECT Id,provider_company,mobile_number,value,invoice_price,charge_status,status_transfer,UserId,CenterId,AddedTime,UserPayedId,cause,Center_Name,masary_trans_id,voucher_number,serial_number,provider,ServId,InCome,InComeStatus,IncomeBalanceID,InCome_cash,Log_Id,RequestID,DenominationID FROM electronic_charge_voucher WHERE CAST(AddedTime AS DATE )  BETWEEN '{date_from}' AND '{date_to}' """,

     'TRANSACTIONS' : """SELECT TransactionID,AccountIDFrom,AccountIDTo,TotalAmount,TransactionType,IsReversed,OriginalTrx,Date,OriginalAmount,Fees,InvoiceID,RequestID,ID,BalanceBefore FROM Transactions WHERE CAST(Date AS DATE )  BETWEEN '{date_from}' AND '{date_to}' """,

     'INVOICE' : """SELECT Id,Invoice_Id_card,ClientName,ClientPhone,ServId,ServCount,TotalPrice,AttachedCount,UserId,AddedTime,Status,InCome,InComeStatus,DeliveryCost,TransInv,IncomeBalanceID,added_money,cause,faw_BillerId,faw_BillTypeCode,faw_FPTN,faw_FCRN,faw_paid,faw_fees,faw_status_code,faw_status_message,Is_fawry,Basci_val,Provider_Response,account_number,service_id,Code,Message,ProviderTransactionId,data,tel_code,tel_number,is_retry,Payed_time,ProviderCardTransactionId,ECardmomknPaymentId,InCome_cash FROM Invoice WHERE CAST(AddedTime AS DATE )  BETWEEN '{date_from}' AND '{date_to}' """,

     'BALANCE'  : """SELECT Id,CenterId,Amount,Type,Descrp,AddedTime,UserId,InvId,Before,ServId,trans_id,added_money,total_invoices,total_invoices_date,tel FROM Balance WHERE CAST(AddedTime AS DATE )  BETWEEN '{date_from}' AND '{date_to}' """,

     'CHANNELTYPES' : """SELECT ID,Name,ArName,ChannelCategoryID,Version FROM ChannelTypes""",

     'ACCOUNTCHANNELS' : """SELECT ID,AccountID,ChannelID,CreationDate,CreatedBy,Status,UpdateDate,UpdatedBy FROM AccountChannels""",

     'CHANNELIDENTIFIERS'  : """SELECT ID,ChannelID,Value,CreationDate,CreatedBy,Status,UpdateDate,UpdatedBy FROM ChannelIdentifiers""",

     'CHANNELS' : """SELECT ID,Name,ChannelTypeID,ChannelOwnerID,Serial,PaymentMethodID FROM Channels""",

     'ACCOUNTTRANSACTIONCOMMISSION' : """SELECT  ATC.ID, ATC.TransactionID, ATC.Commission FROM AccountTransactionCommission ATC JOIN Transactions T on T.ID = ATC.TransactionID WHERE CAST(T.Date AS DATE) BETWEEN '{date_from}' AND '{date_to}'  """,

     'RequestProviderDetails':"""SELECT RPD.ID, RPD.RequestID, RPD.ProviderTransactionId, RPD.ProviderFees FROM RequestProviderDetails RPD JOIN Requests R ON R.ID =RPD.RequestID WHERE CAST(R.RequestDate AS Date) BETWEEN '{date_from}' AND '{date_to}' """,

     'CENTERS_STATEMENT':"""SELECT Id,CenterId,Balance,AddedTime FROM Centers_Statement WHERE CAST(AddedTime AS DATE) BETWEEN '{date_from}' AND '{date_to}'""",

     'LOGS':"""SELECT ID,Log,ProviderServiceRequestID,LogTypeID,Date FROM Logs WHERE CAST(Date AS Date) BETWEEN '{date_from}' AND '{date_to}'""",

     'CallBackWE': """ SELECT ID,RequestID,DenominationID,Status,BillingAccount,CallBackDate,ResultCode,WETransaction FROM CallBackWE WHERE CAST(CallBackDate AS Date) BETWEEN '{date_from}' AND '{date_to}'""",

     'PAYMENTTYPE' : """SELECT ID,Name,ArName FROM PaymentType""",

     'ACCOUNTLOCATIONS' : """SELECT ID,AccountID,Longitude,Latitude,CreationDate,UpdateDate FROM AccountLocations""",

     'AccountRelationMapping':""" SELECT ID,AccountID,ParentID,InsertedDateTime FROM AccountRelationMapping  """,

     'AccountRelationMappingHistory':""" SELECT ID,AccountID,ParentID,UpdatedDateTime,ReasonID FROM AccountRelationMappingHistory  """,

     'Electricity_log' : """ SELECT Id,UserId,CenterId,AddedTime,Center_Name,Provider_Response,amount,account_number,service_id,Code,Message,ProviderTransactionId,paymentAmounts,feesAmounts,Request,AccountName,Address,totalDeducts,minAmount,billRecId,InvId,ServicesId,ECardmomknPaymentId,billNumber,transactions_types_id,RequestIdInquiry FROM Electricity_log WHERE CAST(AddedTime AS Date) BETWEEN '{date_from}' AND '{date_to}' """,

     'PengingPaymentCard': """ SELECT ID,PaymentRefInfo,RequestID,CardTypeID,HostTransactionID,PengingPaymentCardStatusID,InsertedDate,UpdatedDate FROM PengingPaymentCard WHERE CAST(InsertedDate AS Date) BETWEEN '{date_from}' AND '{date_to}' """,
     'Reasons':"""SELECT Id,Name FROM Reasons""",
     'VersionTracking':""" SELECT ID,AccountID,Version,UserID,CreationDate,ChannelID,SerialNumber FROM VersionTracking WHERE CAST(CreationDate AS DATE ) BETWEEN '{date_from}' AND '{date_to}' """,
     'PROVIDERSERVICEREQUESTS' : """select * from ProviderServiceRequests where CAST(CreationDate as date) between '{date_from}' and '{date_to}'  """,
     'PROVIDERSERVICEREQUESTSTATUS':""" SELECT * FROM ProviderServiceRequestStatus """,
     'ChannelCategories' : """select ID,Name,ArName from ChannelCategories""",
     'ChannelOwners' : """select ID,Name,ArName from ChannelOwners""",
     'ServiceEntity' : """select ID,Name,ArName from ServiceEntity""",
     'Balance_Credit' : """SELECT [Id]
                        ,[CenterId]
                        ,[Amount]
                        ,[Type]
                        ,[Descrp]
                        ,[AddedTime]
                        ,[UserId]
                        ,[InvId]
                        ,[Before]
                        ,[ServId]
                        ,[trans_id]
                        ,[added_money]
                        ,[total_invoices]
                        ,[total_invoices_date]
                        ,[tel]
                        ,[Invoice_cnt]
                        ,[Invoice_income]
                        ,[Charge_cnt]
                        ,[Charge_income]
                        ,[Voucher_cnt]
                        ,[Voucher_income]
                        ,[Total_Amount]
                        ,[Invoice]
                        ,[Charge]
                        ,[Voucher]
                        ,[deposit]
                        ,[Transfer_from]
                        ,[Transfer_to]
                        ,[TotalPoint]
                    FROM [Balance_Credit]
                    where CAST(AddedTime as date) between '{date_from}' and '{date_to}'  """,
     'ResetPasswordHistory' : """select * from ResetPasswordHistory""" ,
     
     
}
#*****************************************************************************************
GW_UAT_Tables={

     'INVOICEDETAILS' : """SELECT IDS.Id , IDS.InvId, IDS.SFId, IDS.Value  FROM InvoiceDetails IDS JOIN Invoice I ON IDS.InvId = I.Id WHERE CAST(I.AddedTime AS DATE )  BETWEEN '{date_from}' AND '{date_to}' """,

     'REQUESTS' : """SELECT ID,UUID,AccountID,ServiceDenominationID,Status,TransactionID,UserID,RRN,RequestDate,ResponseDate,Amount,BillingAccount,ChannelID,ProviderServiceRequestID FROM Requests WHERE CAST(RequestDate AS DATE )  BETWEEN '{date_from}' AND '{date_to}'""",

     'ELECTRONIC_CHARGE'  : """SELECT Id,provider_company,mobile_number,value,invoice_price,charge_status,status_transfer,UserId,CenterId,AddedTime,UserPayedId,cause,Center_Name,provider,ServId,InCome,InComeStatus,IncomeBalanceID,Provider_Response,charge,Log_Id,InCome_cash FROM electronic_charge WHERE CAST(AddedTime AS DATE )  BETWEEN '{date_from}' AND '{date_to}'""",

     'ELECTRONIC_CHARGE_VOUCHER' : """SELECT Id,provider_company,mobile_number,value,invoice_price,charge_status,status_transfer,UserId,CenterId,AddedTime,UserPayedId,cause,Center_Name,masary_trans_id,voucher_number,serial_number,provider,ServId,InCome,InComeStatus,IncomeBalanceID,InCome_cash,Log_Id,RequestID,DenominationID FROM electronic_charge_voucher WHERE CAST(AddedTime AS DATE )  BETWEEN '{date_from}' AND '{date_to}'""",

     'TRANSACTIONS' : """SELECT TransactionID,AccountIDFrom,AccountIDTo,TotalAmount,TransactionType,IsReversed,OriginalTrx,Date,OriginalAmount,Fees,InvoiceID,RequestID,ID,BalanceBefore FROM Transactions WHERE CAST(Date AS DATE )  BETWEEN '{date_from}' AND '{date_to}' """,

    'INVOICE' : """SELECT Id,Invoice_Id_card,ClientName,ClientPhone,ServId,ServCount,TotalPrice,AttachedCount,UserId,AddedTime,Status,InCome,InComeStatus,DeliveryCost,TransInv,IncomeBalanceID,added_money,cause,faw_BillerId,faw_BillTypeCode,faw_FPTN,faw_FCRN,faw_paid,faw_fees,faw_status_code,faw_status_message,Is_fawry,Basci_val,Provider_Response,account_number,service_id,Code,Message,ProviderTransactionId,data,tel_code,tel_number,is_retry,Payed_time,ProviderCardTransactionId,ECardmomknPaymentId,InCome_cash FROM Invoice WHERE CAST(AddedTime AS DATE )  BETWEEN '{date_from}' AND '{date_to}' """,

    'BALANCE'  : """SELECT Id,CenterId,Amount,Type,Descrp,AddedTime,UserId,InvId,Before,ServId,trans_id,added_money,total_invoices,total_invoices_date,tel FROM Balance WHERE CAST(AddedTime AS DATE )  BETWEEN '{date_from}' AND '{date_to}' """,

    'ACCOUNTTRANSACTIONCOMMISSION' : """SELECT  ATC.ID, ATC.TransactionID, ATC.Commission FROM AccountTransactionCommission ATC JOIN Transactions T on T.ID = ATC.TransactionID WHERE CAST(T.Date AS DATE) BETWEEN '{date_from}' AND '{date_to}'  """,

    'RequestProviderDetails':"""SELECT RPD.ID, RPD.RequestID, RPD.ProviderTransactionId, RPD.ProviderFees FROM RequestProviderDetails RPD JOIN Requests R ON R.ID =RPD.RequestID WHERE CAST(R.RequestDate AS Date) BETWEEN '{date_from}' AND '{date_to}' """,

    'CENTERS_STATEMENT':"""SELECT Id,CenterId,Balance,AddedTime FROM Centers_Statement WHERE CAST(AddedTime AS DATE) BETWEEN '{date_from}' AND '{date_to}'""",

    'LOGS':"""SELECT ID,Log,ProviderServiceRequestID,LogTypeID,Date FROM Logs WHERE CAST(Date AS Date) BETWEEN '{date_from}' AND '{date_to}'""",

    'CallBackWE': """ SELECT ID,RequestID,DenominationID,Status,BillingAccount,CallBackDate,ResultCode,WETransaction FROM CallBackWE WHERE CAST(CallBackDate AS Date) BETWEEN '{date_from}' AND '{date_to}'""",
    
    'PROVIDERSERVICEREQUESTS' : """select * from ProviderServiceRequests where CAST(CreationDate as date) between '{date_from}' and '{date_to}' 
     """

}

#-------------------------------------------------------------
ID_Tables= {

    "CENTERS" : """SELECT ID,CreationDate,UpdateDate,Address,TaxNo,Active,Parent_CenterID,Total_Parent_Amount,ProfitDailyControl,CreatedBy,Latitude,Name,UpdateBy,Longitude,CommercialRegistrationNo,ActivityID,AccountTypeProfileID,RegionID,EntityID,AccountCode FROM Accounts""",
        
    "ACCOUNTTYPE" : """SELECT ID,CreationDate,UpdateDate,Name,NameAr,Status,TreeLevel FROM AccountTypes """ , 
        
    "ACCOUNTCHANNELS" : """ SELECT ID,CreationDate,UpdateDate,AccountID,ChannelID,Status,CreatedBy,UpdatedBy FROM AccountChannels """ ,    
        
    "AccountTypeProfiles" : """SELECT ID,CreationDate,UpdateDate,AccountTypeID,ProfileID FROM AccountTypeProfiles """,

    "CHANNELS": """ SELECT ID,CreationDate,UpdateDate,Name,ChannelTypeID,ChannelOwnerID,Serial,PaymentMethodID FROM Channels """ ,  
        
    "ACCOUNTLOCATIONS" : """SELECT ID,CreationDate,UpdateDate,Name,GovernorateID FROM Regions """ ,

    "CHANNELTYPES" : """ SELECT ID,Name,ArName,ChannelCategoryID,Version FROM ChannelTypes """ , 
        
    "CHANNElIDENTIFIERS" : """ SELECT ID,ChannelID,Value,CreationDate,CreatedBy,Status,UpdateDate,UpdatedBy FROM ChannelIdentifiers """ ,

    "Governorates" : """ SELECT ID,CreationDate,UpdateDate,Name FROM Governorates """ , 
        
    "Regions" : """ SELECT ID,CreationDate,UpdateDate,Name,GovernorateID FROM Regions """ ,
    "Account_Owners" : """ SELECT ID,CreationDate,UpdateDate,Name,Mobile,Address,Email,NationalID,AccountID FROM AccountOwners  """ 

 }
#----------------------------------------------------------------------------------
TMS_PROD_Tables = {
    
    "AccountFees" : """SELECT ID,CreationDate,UpdateDate,AccountID,FeesID,DenominationID FROM AccountFees""",

    "Fees" : """SELECT ID,CreationDate,UpdateDate,AmountFrom,AmountTo,PaymentModeID,Value,Status,CreatedBy,UpdatedBy,StartDate,EndDate,FeesTypeID,Max,Min FROM Fees""",
    
    "AccountTransactionCommissions" : """SELECT ID,CreationDate,UpdateDate,TransactionID,Commission FROM AccountTransactionCommissions WHERE CAST(CreationDate AS Date) BETWEEN '{date_from}' AND '{date_to}'""",
    
    "FailedRequestStatusCodes" : """SELECT ID,CreationDate,UpdateDate,Code,Message,RequestID FROM FailedRequestStatusCodes WHERE CAST(CreationDate AS Date) BETWEEN '{date_from}' AND '{date_to}'""" , 
        
    "InquiryBillDetails" : """ SELECT ID,CreationDate,UpdateDate,InquiryBillID,Value,ProviderDenominationParameterID FROM InquiryBillDetails WHERE CAST(CreationDate AS Date) BETWEEN '{date_from}' AND '{date_to}'""" ,    
        
    "InquiryBills" : """SELECT ID,CreationDate,UpdateDate,Amount,Sequence,ProviderServiceResponseID,Mandatory,MaxAmount,MinAmount,OpenAmount FROM InquiryBills WHERE CAST(CreationDate AS Date) BETWEEN '{date_from}' AND '{date_to}'""",

    "Logs": """ SELECT ID,CreationDate,UpdateDate,LogText,ProviderServiceRequestID,LogTypeID FROM Logs WHERE CAST(CreationDate AS Date) BETWEEN '{date_from}' AND '{date_to}'""" ,  
        
    "ProviderServiceRequests" : """  SELECT ID,CreationDate,UpdateDate,RequestTypeID,ProviderServiceRequestStatusID,Brn,DenominationID,CreatedBy,UpdatedBy,BillingAccount,CreationDateSQL FROM ProviderServiceRequests WHERE CAST(CreationDate AS Date) BETWEEN '{date_from}' AND '{date_to}'""" ,

    "ProviderServiceResponses" : """ SELECT ID,CreationDate,UpdateDate,ProviderServiceRequestID,TotalAmount,CreationDateSQL FROM ProviderServiceResponses WHERE CAST(CreationDate AS Date) BETWEEN '{date_from}' AND '{date_to}'""" , 
        
    "ProviderServiceResponseParams" : """ SELECT ID,CreationDate,UpdateDate,ProviderServiceResponseID,Value,ParameterID,Brn,CreationDateSQL FROM ProviderServiceResponseParams WHERE CAST(CreationDate AS Date) BETWEEN '{date_from}' AND '{date_to}'""" , 
        
    "ProviderServiceRequestParams" : """ SELECT ID,CreationDate,UpdateDate,ProviderServiceRequestID,Value,ParameterID,CreationDateSQL FROM ProviderServiceRequestParams WHERE CAST(CreationDate AS Date) BETWEEN '{date_from}' AND '{date_to}'""" , 
        
    "ProviderTransactionResponses" : """ SELECT ID,CreationDate,UpdateDate,TransactionID,ProviderTransactionID,RequestID FROM ProviderTransactionResponses WHERE CAST(CreationDate AS Date) BETWEEN '{date_from}' AND '{date_to}'""" ,    
        
    "ProviderDenominationParameters" : """SELECT ID,CreationDate,UpdateDate,Name,ArName,IsInquiryDisplayed,DenominationID,ParameterID,Source FROM ProviderDenominationParameters """,

    "ReceiptBodyParams": """ SELECT ID,CreationDate,UpdateDate,ProviderServiceRequestID,Value,TransactionID,ProviderDenominationParameterID,CreationDateSQL FROM ReceiptBodyParams WHERE CAST(CreationDate AS Date) BETWEEN '{date_from}' AND '{date_to}'""" ,  
        
    "Requests" : """  SELECT ID,CreationDate,UpdateDate,UUID,AccountID,ServiceDenominationID,StatusID,TransactionID,UserID,RRN,ResponseDate,Amount,BillingAccount,ChannelID,ProviderServiceRequestID,DenominationServiceProviderID,PaymentMethod,AccountProfileId,RefNumber,CompositeTransactionID,CreationDateSQL FROM Requests WHERE CAST(CreationDate AS Date) BETWEEN '{date_from}' AND '{date_to}'""" ,

    "Transactions" : """ SELECT ID,CreationDate,UpdateDate,AccountIDFrom,AccountIDTo,TotalAmount,OriginalAmount,Fees,TransactionType,IsReversed,OriginalTrx,InvoiceID,RequestID,TransactionID,Taxes,CreationDateSQL FROM Transactions WHERE CAST(CreationDate AS Date) BETWEEN '{date_from}' AND '{date_to}'""" , 
        
    "TransferCategories" : """ SELECT ID,CreationDate,UpdateDate,CategoryName,CatgoryNameAr FROM TransferCategories """ , 
        
    "TransferSubCategories" : """ SELECT ID,CreationDate,UpdateDate,SubCategoryName,SubCategoryNameAr,TransferCategoryID,DenominationID,AccountID FROM TransferSubCategories """ , 
        
    "TransferRequests" : """ SELECT ID,CreationDate,UpdateDate,TransferSubCategoryID,DueDate,Amount,AccountIDFrom,AccountIDTo,Reason,RequestId,UserID FROM TransferRequests WHERE CAST(CreationDate AS Date) BETWEEN '{date_from}' AND '{date_to}'""", 

    "DenominationServiceProviders":""" SELECT ID,CreationDate,UpdateDate,DenominationID,ServiceProviderID,Balance,Status,ProviderAmount,ProviderHasFees,ProviderCode FROM DenominationServiceProviders """,

    "ServiceProviders":""" SELECT ID,CreationDate,UpdateDate,Name,Code FROM ServiceProviders  """,

    "Services" : """SELECT ID,CreationDate,UpdateDate,Name,ArName,ServiceEntityID,Status,ServiceTypeID,ServiceGroupID FROM Services""",

    "Denominations": """SELECT ID,CONVERT(VARCHAR, CreationDate, 120) AS [CreationDate],CONVERT(VARCHAR, UpdateDate, 120) AS [UpdateDate],Name,Value,ServiceID,PaymentModeID,Status,CurrencyID,ServiceCategoryID,Inquirable,BillPaymentModeID,ArName,Code FROM Denominations"""
}

TMS_UAT_Tables = {
    
    "AccountTransactionCommissions" : """SELECT ID,CreationDate,UpdateDate,TransactionID,Commission FROM AccountTransactionCommissions WHERE CAST(CreationDate AS Date) BETWEEN '{date_from}' AND '{date_to}'""",
    
    "FailedRequestStatusCodes" : """SELECT ID,CreationDate,UpdateDate,Code,Message,RequestID FROM FailedRequestStatusCodes WHERE CAST(CreationDate AS Date) BETWEEN '{date_from}' AND '{date_to}'""" , 
        
    "InquiryBillDetails" : """ SELECT ID,CreationDate,UpdateDate,InquiryBillID,Value,ProviderDenominationParameterID FROM InquiryBillDetails WHERE CAST(CreationDate AS Date) BETWEEN '{date_from}' AND '{date_to}'""" ,    
        
    "InquiryBills" : """SELECT ID,CreationDate,UpdateDate,Amount,Sequence,ProviderServiceResponseID,Mandatory,MaxAmount,MinAmount,OpenAmount FROM InquiryBills WHERE CAST(CreationDate AS Date) BETWEEN '{date_from}' AND '{date_to}'""",

    "Logs": """ SELECT ID,CreationDate,UpdateDate,LogText,ProviderServiceRequestID,LogTypeID FROM Logs WHERE CAST(CreationDate AS Date) BETWEEN '{date_from}' AND '{date_to}'""" ,
        
    "ProviderServiceRequests" : """  SELECT ID,CreationDate,UpdateDate,RequestTypeID,ProviderServiceRequestStatusID,Brn,DenominationID,CreatedBy,UpdatedBy,BillingAccount FROM ProviderServiceRequests WHERE CAST(CreationDate AS Date) BETWEEN '{date_from}' AND '{date_to}'""" ,

    "ProviderServiceResponses" : """ SELECT ID,CreationDate,UpdateDate,ProviderServiceRequestID,TotalAmount FROM ProviderServiceResponses WHERE CAST(CreationDate AS Date) BETWEEN '{date_from}' AND '{date_to}'""" , 
        
    "ProviderServiceResponseParams" : """ SELECT ID,CreationDate,UpdateDate,ProviderServiceResponseID,Value,ParameterID,Brn FROM ProviderServiceResponseParams WHERE CAST(CreationDate AS Date) BETWEEN '{date_from}' AND '{date_to}'""" , 
        
    "ProviderServiceRequestParams" : """ SELECT ID,CreationDate,UpdateDate,ProviderServiceRequestID,Value,ParameterID FROM ProviderServiceRequestParams WHERE CAST(CreationDate AS Date) BETWEEN '{date_from}' AND '{date_to}'""" , 
        
    "ProviderTransactionResponses" : """ SELECT ID,CreationDate,UpdateDate,TransactionID,ProviderTransactionID,RequestID FROM ProviderTransactionResponses WHERE CAST(CreationDate AS Date) BETWEEN '{date_from}' AND '{date_to}'""" ,    

    "ReceiptBodyParams": """ SELECT ID,CreationDate,UpdateDate,ProviderServiceRequestID,Value,TransactionID,ProviderDenominationParameterID FROM ReceiptBodyParams WHERE CAST(CreationDate AS Date) BETWEEN '{date_from}' AND '{date_to}'""" ,  
        
    "Requests" : """  SELECT ID,CreationDate,UpdateDate,UUID,AccountID,ServiceDenominationID,StatusID,TransactionID,UserID,RRN,ResponseDate,Amount,BillingAccount,ChannelID,ProviderServiceRequestID,DenominationServiceProviderID,PaymentMethod,AccountProfileId,RefNumber,CompositeTransactionID FROM Requests WHERE CAST(CreationDate AS Date) BETWEEN '{date_from}' AND '{date_to}'""" ,

    "Transactions" : """ SELECT ID,CreationDate,UpdateDate,AccountIDFrom,AccountIDTo,TotalAmount,OriginalAmount,Fees,TransactionType,IsReversed,OriginalTrx,InvoiceID,RequestID,TransactionID,Taxes FROM Transactions WHERE CAST(CreationDate AS Date) BETWEEN '{date_from}' AND '{date_to}'""" , 
        
    "TransferRequests" : """ SELECT ID,CreationDate,UpdateDate,TransferSubCategoryID,DueDate,Amount,AccountIDFrom,AccountIDTo,Reason,RequestId,UserID FROM TransferRequests WHERE CAST(CreationDate AS Date) BETWEEN '{date_from}' AND '{date_to}'""" 
    
}
#----------------------------------------------------------------------
SOF_PROD_Tables = {

    "ACCOUNTSERVICEAVAILABLEBALANCES" : """SELECT ID,CreationDate,UpdateDate,AccountID,Balance,BalanceTypeID,AccountBalanceTypeID,CreditLimit FROM AccountServiceAvailableBalances """,
        
    "ACCOUNTSERVICEBALANCES" : """SELECT ID,CreationDate,UpdateDate,AccountID,Balance,BalanceTypeID,AccountBalanceTypeID,CreditLimit FROM ACCOUNTSERVICEBALANCES """ , 
        
    "BalanceHistories" : """ SELECT ID,CreationDate,UpdateDate,TransactionID,BalanceBefore,AccountID,BalanceTypeID,TotalBalance,RequestID,AvaialableBalanceAfter,ActionID,CreationDateSQL FROM BalanceHistories WHERE CAST(CreationDate AS Date) BETWEEN '{date_from}' AND '{date_to}'""" ,    
        
    "HoldBalances" : """SELECT ID,CreationDate,UpdateDate,AccountID,RequestID,Amount,SourceID,Status,AvailableBalanceBefore,BalanceTypeID,ConfirmedAmount,CreditLimitBefore,CreationDateSQL FROM HoldBalances WHERE CAST(CreationDate AS Date) BETWEEN '{date_from}' AND '{date_to}'"""
}

SOF_UAT_Tables = {
        
    "BalanceHistories" : """ SELECT ID,CreationDate,UpdateDate,TransactionID,BalanceBefore,AccountID,BalanceTypeID,TotalBalance,RequestID,AvaialableBalanceAfter,ActionID FROM BalanceHistories WHERE CAST(CreationDate AS Date) BETWEEN '{date_from}' AND '{date_to}'""" ,    
        
    "HoldBalances" : """SELECT ID,CreationDate,UpdateDate,AccountID,RequestID,Amount,SourceID,Status,AvailableBalanceBefore,BalanceTypeID,ConfirmedAmount,CreditLimitBefore FROM HoldBalances WHERE CAST(CreationDate AS Date) BETWEEN '{date_from}' AND '{date_to}' """
}

ReportsDB_PROD_Tables = {

    "AcquirerBankFees" : """SELECT * FROM [dbo].[AcquirerBankFees]""",

    "BAF" : """SELECT * FROM [dbo].[BAF]""",

    "BillerShare" : """SELECT * FROM [dbo].[BillerShare]""",

    "MDR" : """SELECT * FROM [dbo].[MDR]""",

    "ParentGroups" : """SELECT * FROM [dbo].[ParentGroups]""",

    "ServiceType": """SELECT * FROM [dbo].[ServiceType]""",

    "Centers_Statement":"""SELECT * FROM Centers_Statement  WHERE CreationDate = '{today_date}'"""
}

ReportsDB_UAT_Tables = {

    "Centers_Statement":"""SELECT * FROM Centers_Statement WHERE CreationDate = '{today_date}'"""
}

#-------------------------------------------------------------------
swicth_Tables = {
    'SW_PROD_REQUESTS' : """SELECT ID,REQUEST_ID,TRANSACTION_ID,STATUS,SERVICE_IDENTIFIER,REQUEST_TIME,RESPONSE_TIME FROM momkn_callback.requests WHERE "REQUEST_TIME" BETWEEN TO_DATE('{date_from}', 'MM-DD-YYYY HH24:MI:SS') AND TO_DATE('{date_to}', 'MM-DD-YYYY HH24:MI:SS') """
}


#**************************************************************************************
DimensionTables = [ 'GW_PROD_CENTERS',"GW_PROD_SERVICEPROVIDER", "GW_PROD_PARENTGROUPS", 
                   'GW_PROD_SERVICEGROUPS', 'GW_PROD_SERVICE', 'GW_PROD_SERVICES',
                   'GW_PROD_TRANSACTIONS_TYPES', 'GW_PROD_PROVIDER', 'GW_PROD_DENOMINATIONS', 
                   'GW_PROD_DENOMINATIONSERVICEPROVIDER', 'GW_PROD_PLACE', 'GW_PROD_ACCOUNTTYPE', 
                   'GW_PROD_SERVICESFIELDS', 'GW_PROD_REQUESTSTATUS', 'GW_PROD_CHANNELTYPES', 
                   'GW_PROD_ACCOUNTCHANNELS', 'GW_PROD_CHANNELIDENTIFIERS', 'GW_PROD_CHANNELS', 
                   'GW_PROD_PAYMENTTYPE', 'GW_PROD_ACCOUNTLOCATIONS',
                   'GW_PROD_AccountRelationMapping', 'GW_PROD_AccountRelationMappingHistory',
                   "ID_PROD_CENTERS", "ID_PROD_ACCOUNTTYPE", "ID_PROD_ACCOUNTCHANNELS", "ID_PROD_AccountTypeProfiles", 
                   "ID_PROD_CHANNELS", "ID_PROD_ACCOUNTLOCATIONS", "ID_PROD_CHANNELTYPES",
                   "ID_PROD_CHANNElIDENTIFIERS", "ID_PROD_Governorates", "ID_PROD_Regions",
                   "ID_PROD_Account_Owners", "TMS_PROD_AccountFees","TMS_PROD_Fees",
                   "TMS_PROD_ProviderDenominationParameters",
                   "TMS_PROD_TransferCategories", "TMS_PROD_TransferSubCategories",
                   "TMS_PROD_DenominationServiceProviders","TMS_PROD_ServiceProviders",
                   "TMS_PROD_Services","TMS_PROD_Denominations",
                   "SOF_PROD_ACCOUNTSERVICEAVAILABLEBALANCES", "SOF_PROD_ACCOUNTSERVICEBALANCES",
                   "ReportsDB_AcquirerBankFees", "ReportsDB_BAF", "ReportsDB_BillerShare", 
                   "ReportsDB_MDR", "ReportsDB_ParentGroups","ReportsDB_ServiceType","GW_PROD_Reasons","GW_PROD_PROVIDERSERVICEREQUESTSTATUS",
                   'GW_PROD_ChannelCategories','GW_PROD_ChannelOwners','GW_PROD_ServiceEntity','GW_PROD_ResetPasswordHistory']
#____________________________________________________________________________________________________________________________________

def format_queries(dict, date_from, date_to):  #concating dating value in queries
    for key, value in dict.items():
        formatted_query = value.format(date_from = date_from, date_to = date_to)
        dict[key] = formatted_query


def Replacing_with_dates(list ,date_from ,date_to ,oracle_date_from,oracle_date_to,today_date ): #applying the function on all the srcs
    for dictionary in list :
        if dictionary == swicth_Tables:
            format_queries(dictionary, oracle_date_from, oracle_date_to)
        elif dictionary == ReportsDB_PROD_Tables or dictionary == ReportsDB_UAT_Tables:
            for key,value in dictionary.items():
                dictionary[key] = value.format(today_date = today_date)
        else :
            format_queries(dictionary, date_from, date_to)
#____________________________________________________________________________________

def Reading_STAGING(o_user , o_pass , DimensionTables): #reading from staging the dimension tables.
    Dim_dict = {}
    for value in  DimensionTables:
            table_name = 'STAGING'+'.'+ value
            dfw = spark.read.format('jdbc').option('driver' ,'oracle.jdbc.driver.OracleDriver' )\
            .option('url' ,'jdbc:oracle:thin:@10.90.6.130:1521:dwhdb')\
            .option('user' ,o_user)\
            .option('password' , o_pass)\
            .option('dbtable',table_name).load()
            print('Table {} has been read successfully '.format(value))
            Dim_dict[value] = dfw
    return Dim_dict
    
    
 #------------------------------------------------------------------------------------
def Reading_from_Switch(s_user , s_pass , Query):
    SW_DF=spark.read.format('jdbc').option('driver' ,'oracle.jdbc.driver.OracleDriver' )\
      .option('url' ,'jdbc:oracle:thin:@10.90.3.198:1521:cachcollect')\
      .option('user' ,s_user)\
      .option('password' , s_pass)\
      .option('query',Query)\
      .load()
    return SW_DF
#_____________________________________________________________________________________________



def Reading_From_MSSQL(Suser ,Spass,server ,db, Query):
    jdbc_url = "jdbc:sqlserver://"+server+":1433;DatabaseName="+db+";encrypt=true;trustServerCertificate=true;trusted_connection=no;applicationIntent=readOnly"
    df = spark.read \
          .format("jdbc") \
          .option("url",jdbc_url) \
          .option("query",  Query) \
          .option("user", Suser) \
          .option("password", Spass) \
          .option("encoding", "utf-8")\
          .option("driver","com.microsoft.sqlserver.jdbc.SQLServerDriver")\
          .load()        
    return  df


#_______________________________________________________________________________________

def Reading_From_MSSQL(Suser ,Spass,server ,db, Query):
    jdbc_url = "jdbc:sqlserver://"+server+":1433;DatabaseName="+db+";encrypt=true;trustServerCertificate=true;trusted_connection=no;applicationIntent=readOnly"
    df = spark.read \
          .format("jdbc") \
          .option("url",jdbc_url) \
          .option("query",  Query) \
          .option("user", Suser) \
          .option("password", Spass) \
          .option("driver","com.microsoft.sqlserver.jdbc.SQLServerDriver")\
          .load()        
    return  df
#_____________________________________________________________________________________________
def Reading_from_allSources(list_dbs ,MS_sql_db_username , MS_sql_db_password ,new_sys_user , new_sys_pass ,sw_user, sw_pass,new_uat_user,new_uat_pass):
    Dict = {}
    for db in list_dbs :
      
        if db == 'Momkn_Pro':
            try:
            #reading data form getway production
                for key, query in GW_PROD_Tables.items():
                    key = 'GW_PROD_' + key
                    df = Reading_From_MSSQL(MS_sql_db_username , MS_sql_db_password, '10.90.3.221' ,'momken', query) 
                    Dict[key] = df 
                print('momkn done')  
            except:
                print('Error in DB!!!!!!!' , db)
                DimensionTables.remove(key)

        elif db == 'Momkn_UAT':
            try:
            #reading data form getway UAT
                for key, query in GW_UAT_Tables.items():
                    key = 'GW_UAT_' + key
                    df = Reading_From_MSSQL(MS_sql_db_username , MS_sql_db_password, '10.90.3.146' ,'momken', query) 
                    Dict[key] = df
                print('momkn UAT done')  
            except :
                print('Error in DB!!!!!!!' , db)
                DimensionTables.remove(key)
        elif db == 'IdentityServer_Pro':
            #reading data form IdentityServer_Production
            try:
                for key , query in ID_Tables.items():
                    key = 'ID_PROD_' + key
                    df = Reading_From_MSSQL(new_sys_user , new_sys_pass , 'NewSysN3' ,'IdentityServer', query) 
                    Dict[key] = df 
                print('Identity done') 
            except:
                print('Error in DB!!!!!!!' , db)
                DimensionTables.remove(key)
        elif db == 'TMS_Pro' :
            try:
            #reading data form TMS_Production
                for key , query in TMS_PROD_Tables.items():
                    key = 'TMS_PROD_' + key
                    df = Reading_From_MSSQL(new_sys_user , new_sys_pass, 'NewSysN3' , 'TMSLite', query)    
                    Dict[key] = df  
                print('TMS done') 
            except:
                print('Error in DB!!!!!!!' , db)
                DimensionTables.remove(key)
        elif db == 'TMS_UAT' :
            try:
                #reading data form TMS_UAT
                for key , query in TMS_UAT_Tables.items():
                    key = 'TMS_UAT_' + key
                    df = Reading_From_MSSQL(new_uat_user,new_uat_pass, '10.90.4.20' , 'TMS', query)    
                    Dict[key] = df 
            except:
                 print('Error in DB!!!!!!!' , db)
                 DimensionTables.remove(key)
        elif db == 'SourceOfFund_Pro' :
            try:
                #reading data form SourceOfFund production
                for key , query in SOF_PROD_Tables.items():
                    key = 'SOF_PROD_' + key
                    df = Reading_From_MSSQL(new_sys_user ,new_sys_pass , 'NewSysN3' , 'SourceOfFund', query)    
                    Dict[key] = df 
                print('SourceOfFund_Pro done') 
            except:
                print('Error in DB!!!!!!!' , db)
                DimensionTables.remove(key) 
        elif db == 'SourceOfFund_UAT' :
            try:
                #reading data form SourceOfFund production
                for key , query in SOF_UAT_Tables.items():
                    key = 'SOF_UAT_' + key
                    df = Reading_From_MSSQL(new_uat_user,new_uat_pass, '10.90.4.20' , 'SourceOfFund', query)    
                    Dict[key] = df
                print('SOF_UAT done') 

            except:
                print('Error in DB!!!!!!!' , db)
                DimensionTables.remove(key)
        elif db == 'ReportsDB_PROD':
            try:
                #reading data form ReportsDB
                for key , query in ReportsDB_PROD_Tables.items():
                    key = 'ReportsDB_' + key
                    df = Reading_From_MSSQL(new_sys_user ,new_sys_pass, 'NewSysN3' , 'ReportsDB', query)    
                    Dict[key] = df
                print('ReportsDB PROD done') 

            except:
                print('Error in DB!!!!!!!' , db)
                DimensionTables.remove(key)
        elif db == 'ReportsDB_UAT':
            try:
                #reading data form ReportsDB
                for key , query in ReportsDB_UAT_Tables.items():
                    key = 'ReportsDB_UAT_' + key
                    df = Reading_From_MSSQL(new_uat_user,new_uat_pass, '10.90.4.20' , 'ReportsDB', query)    
                    Dict[key] = df
                print('ReportsDB UAT done') 

            except:
                print('Error in DB!!!!!!!' , db)
                DimensionTables.remove(key)
                
        else :
            try:
                #reading data form Switch
                for key , query in swicth_Tables.items():
                    df = Reading_from_Switch(sw_user, sw_pass, query)
                    Dict[key] = df  
                print('swicth done')
            except:
                print('Error in DB!!!!!!!' , db)
                DimensionTables.remove(key) 
    return Dict


def Invoice_Cleaning(list , Dict):
    for name in list:
        if name == 'Is_Fawry' or name=='InComeStatus':
            Dict['GW_PROD_INVOICE'] = Dict['GW_PROD_INVOICE'].withColumn(name ,when( col(name) == True , 1).otherwise(0))
            Dict['GW_UAT_INVOICE'] = Dict['GW_UAT_INVOICE'].withColumn(name ,when( col(name) == True , 1).otherwise(0))
    list.remove('Is_Fawry')
    list.remove('InComeStatus')
    return Dict , list

def Centers_Cleaning(list , Dict):

     for name in list:
         if name == 'Active' or name == 'ProfitDailyControl':
            Dict['ID_PROD_CENTERS'] = Dict['ID_PROD_CENTERS'].withColumn(name,  col(name).cast('int'))
            Dict['GW_PROD_CENTERS'] = Dict['GW_PROD_CENTERS'].withColumn(name , col(name).cast('int'))

         elif name == 'Address' or name == 'CenterName':
            Dict['GW_PROD_CENTERS'] = Dict['GW_PROD_CENTERS'].withColumn(name , ltrim(col(name))).withColumn(name , rtrim(col(name)))

         elif name == 'AllowSelfTransfer':
            Dict['GW_PROD_CENTERS'] = Dict['GW_PROD_CENTERS'].withColumn(name,  col(name).cast('int'))

         else:
            Dict['GW_PROD_CENTERS'] = Dict['GW_PROD_CENTERS'].withColumn(name,  col(name).cast('int'))
     return Dict
     

#------------------------------------------------------------------------------------------------------    

def clean_all_dataframes(Dict):
    cleaned_dataframes = {}
    
    for key, dataframe in Dict.items():
        cleaned_columns = []
        for column in dataframe.columns:
            column_type = dataframe.schema[column].dataType
            # Apply regexp_replace only to string columns to avoid type casting issues
            if isinstance(column_type, StringType):
                cleaned_column = regexp_replace(col(column), "[\\n\\t]", "").alias(column)
            else:
                cleaned_column = col(column)
            cleaned_columns.append(cleaned_column)
        
        cleaned_dataframe = dataframe.select(*cleaned_columns)
        cleaned_dataframes[key] = cleaned_dataframe
    
    return cleaned_dataframes
#_______________________________________________________________________________________________________


import jaydebeapi
from pyspark.sql.types import *
from datetime import datetime

def check_and_create_table(user, pass_, schema_name, table_name, sample_df):
    try:
        # Establish connection to Doris
        conn = jaydebeapi.connect(
            'com.mysql.cj.jdbc.Driver',
            f'jdbc:mysql://10.90.6.135:9030/information_schema',
            [user, pass_],
            '/home/Application/Apache_Spark/ETL/Doris/mysql-connector-j-8.2.0.jar'
        )
        curs = conn.cursor()
        
        # Check if the table exists
        curs.execute(f"SELECT COUNT(*) FROM tables WHERE table_schema = '{schema_name}' AND table_name = '{table_name.split('.')[-1]}'") 
        exists = curs.fetchone()[0]
        
        if not exists:
            # Column type mapping
            def map_dtype(dtype):
                if isinstance(dtype, (FloatType, DoubleType,DecimalType)):
                    return 'DECIMAL(38,10)'
                elif isinstance(dtype, StringType):
                    return 'VARCHAR'
                elif isinstance(dtype, LongType):
                    return 'BIGINT' 
                elif isinstance(dtype, TimestampType):
                    return 'DATETIME'  # Map Spark TimestampType to MySQL DATETIME
                elif isinstance(dtype, IntegerType):
                    return 'INT'  # Map Spark IntegerType to MySQL INT
                else:
                    return 'VARCHAR'  # Default to VARCHAR for other types
            
            # Get column names and their mapped types
            columns = [f"{col_name} {map_dtype(sample_df.schema[col_name].dataType)}" for col_name in sample_df.columns]
            columns_str = ', '.join(columns)
            
            # Create table query
            create_query = f"""
                CREATE TABLE {table_name} (
                    {columns_str}
                )
                DISTRIBUTED BY HASH({sample_df.columns[0]}) BUCKETS 10
                PROPERTIES('replication_num' = '1')
            """
            
            # Execute create table query
            curs.execute(create_query)
            print(f"Table {table_name} created successfully.")
        
        # Close cursor and connection
        curs.close()
        conn.close()
    
    except Exception as e:
        print(f"Error while checking or creating table {table_name}: {e}")
        if 'curs' in locals():
            curs.close()
        if 'conn' in locals():
            conn.close()
# Usage example:
# sample_df = ...  # Your Spark DataFrame
# check_and_create_table('user', 'password', 'schema_name', 'table_name', sample_df)
            
#---------------------------------------------------------------------------------------

        
#------------------------------------------------------------------------------------------------------
        

def writting_DIM(user, pass_, schema_name, Dict):
    #tables =  ['GW_PROD_CENTERS']
    for key in DimensionTables:
        #if  key in tables:
        print('Table to be written is: {}'.format(key))
        table_name = schema_name + '.' + key
        sample_df = Dict[key].limit(1)
        check_and_create_table(user, pass_, schema_name, table_name, sample_df)
        Dict[key].write.format("doris") \
        .mode("overwrite") \
        .option("doris.table.identifier", table_name) \
        .option("doris.fenodes", "10.90.6.135:8030") \
        .option("doris.query.port", "9030") \
        .option("user", user) \
        .option("password", pass_) \
        .option("doris.write.replicated", "1") \
        .option("delimiter", "`") \
        .option("sep", "\t") \
        .option("failOnMissingOrIncompatibleColumn", True) \
        .save()
        print(table_name+' '+'loaded successfully')
        del Dict[key]
    return Dict
        
#-----------------------------------------------------------------------------------------------------


def write_to_oracle(user, pass_, schema_name, Dict):
    
 
    excluded_tables = ['GW_PROD_LOGS', 'GW_UAT_LOGS', 'TMS_PROD_Logs', 'TMS_UAT_Logs']
    
    for key, value in Dict.items() :
        if key not in excluded_tables:
           print('Table to be written is: {}'.format(key))
           table_name = schema_name + '.' + key
           sample_df = Dict[key].limit(1)
           check_and_create_table(user, pass_, schema_name, table_name, sample_df)
           table_name = schema_name + '.' + key
           Dict[key].write.format("doris") \
               .mode("append") \
               .option("doris.table.identifier", table_name) \
               .option("doris.fenodes", "10.90.6.135:8030") \
               .option("doris.query.port", "9030") \
               .option("user", user) \
               .option("password", pass_) \
               .option("doris.write.replicated", "1") \
               .option("delimiter", "`") \
               .option("sep", "\t") \
               .option("failOnMissingOrIncompatibleColumn", True) \
               .save()
           print(table_name+' '+'loaded successfully')

#________________________________________________________________________________________________________
def main():

    date_from = datetime.strftime(datetime.now() - timedelta(1), '%m-%d-%Y') 
    date_to = datetime.strftime(datetime.now() - timedelta(1), '%m-%d-%Y')
    #date for new system closing balance
    today_date = datetime.strftime(datetime.now() - timedelta(0), '%Y-%m-%d') + ' 00:00:00.000' 
    #print(today_date)
    oracle_date_from = datetime.strftime(datetime.now() - timedelta(1), '%m-%d-%Y') + '00:00:00'
    oracle_date_to = datetime.strftime(datetime.now() - timedelta(0), '%m-%d-%Y') + '00:00:00'
    #----------------- Load env variable ---------------------------------------------
    load_dotenv()
    MS_sql_db_username = os.getenv("SS_User")
    MS_sql_db_password = os.getenv("SS_Pass")
    new_sys_user = os.getenv("NewPro_User")
    new_sys_pass = os.getenv("NewPro_Pass")
    sw_user = os.getenv("SW_User")
    sw_pass = os.getenv("SW_Pass")
    new_uat_user=os.getenv("NewUAT_User")
    new_uat_pass=os.getenv("NewUAT_Pass")
    #----------------------- Replacing Dates-----------------------------
    listOf_Dicts = [GW_PROD_Tables , GW_UAT_Tables ,ID_Tables , SOF_PROD_Tables ,TMS_PROD_Tables,SOF_UAT_Tables, TMS_UAT_Tables ,ReportsDB_PROD_Tables,ReportsDB_UAT_Tables]
    Replacing_with_dates(listOf_Dicts, date_from ,date_to ,oracle_date_from,oracle_date_to,today_date )
    #---------------------- reading from SQL / Oracle --------------------------------
            
    list_dbs =['Momkn_Pro', 
               'Momkn_UAT', 
               'IdentityServer_Pro' , 
               'TMS_Pro' , 'SourceOfFund_Pro', 
               'TMS_UAT' , 'SourceOfFund_UAT', 'ReportsDB_PROD', 'ReportsDB_UAT']
    Dict = Reading_from_allSources(list_dbs,MS_sql_db_username ,MS_sql_db_password,new_sys_user , new_sys_pass ,sw_user, sw_pass,new_uat_user,new_uat_pass)
    #print(Dict.keys())
    #Dict['GW_PROD_INVOICE'].printSchema()
    #------------------------cleaning columns --------------------------------------
    col_Names =['AllowTransfeer' ,'is_agent' ,'Logged_In' ,
                 'Web','Api' , 'TransferTree',
                 'Active', 'ProfitDailyControl','Address', 'CenterName' , 'Is_Fawry','InComeStatus','AllowSelfTransfer']
    #------------------------ checking dimension-------------------------------------
    STG_oracle_username = os.getenv("STG_User")
    STG_oracle_password = os.getenv("STG_Pass")
    #Dim_Dict = Reading_STAGING(STG_oracle_username, STG_oracle_password, DimensionTables)
    #------------------------------------------------------------------------------
    
    
    cleaned_dictionary = clean_all_dataframes(Dict)
    
    Dict_ ,list =  Invoice_Cleaning(col_Names , cleaned_dictionary)
    prod_Dict =  Centers_Cleaning(list , Dict_)
    
    print('-----------------------------------Dict--------------')
    Dict = writting_DIM('root' , '' , 'STAGING' , prod_Dict)
    
    
    write_to_oracle('root', '', 'STAGING', Dict)
#___________________________________________________________________________________________________________________________
try:
    main()
except:
    raise


